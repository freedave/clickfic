# ClickFic #

### What is this repository for? ###

[ClickFic](http://www.clickfic.com) is a platform for reading and writing interactive fiction on personal computers and mobile devices.  This Git repository contains the codebase for the mobile and desktop applications used for reading the stories.  It includes the ClickFic Library (sub-module **"android"**), which is an Android app containing stories written by the community, and a similar Beta Library (**"android-beta"**) for stories that are in the testing stage.  There is also a desktop PC application and placeholder for a future iOS application.

* The codebase in this repository can be used to develop custom interactive fiction applications (for example, an app on the Google Play store) based on the **"core"** sub-module, under a choice of two licensing terms (see below).
* The mobile application is currently undergoing closed beta testing.  The Beta Library is published to the Google Play store, and will soon be made available for public beta testing.
* Current version: 1.0.0-SNAPSHOT


### Licensing terms
*  This repository is dual licensed to allow both open-source and commercial extendability.  The open source license is the [GNU GPLv3](http://www.gnu.org/licenses/quick-guide-gplv3.en.html), which allows for modification and distribution of the code provided that the application that uses it also adopts a GPL license and open-source their code.  Those who find the GPL too restrictive can purchase a commercial license (see the licensing section of [www.clickfic.com](https://www.clickfic.com)) and have the freedom to modify, distribute and sell their derivative works based off of ClickFic without having to open source their code.
* This repository does not contain the source for the interactive-fiction engine.  **IF-Engine** is an independent library developed in a [separate Git repository](https://bitbucket.org/freedave/ddx-ifengine), and is released under the more permissive Apache v2.0 license.

### How do I set up the project? ###

* This is a Java and Android application based on JRE 1.7+, and Android KitKat (19+).
* The build is based on Gradle, but does not require that the developer install Gradle.
* The Maven build (pom.xml files) is deprecated and no longer supported.
* Documentation to assist in configuring and extending the application is forthcoming.
* Third-party dependencies:
    * [LibGDX](https://libgdx.badlogicgames.com/) version 1.2 -- java-based 2D java graphics engine
    * [Google GSON](https://github.com/google/gson) version 2.2.4 - for model data serialization to JSON format
    * [SLF4J](http://www.slf4j.org/) version 1.7.13 - logging facade
    * [J-Unit](http://junit.org/) version 4.11 - unit testing
    * [Mockito](http://mockito.org/) version 1.9.5 - mocks for unit testing
    * [Hamcrest](http://hamcrest.org/) version 1.3 - matchers for unit testing
    * [GDX-Template](https://bitbucket.org/freedave/ddx-gdx-template) version 0.5.0 - framework for GDX app
    * [IF-Engine](https://bitbucket.org/freedave/ddx-ifengine) version 1.0.0 - interactive fiction engine


### Contribution guidelines and contact info ###

* If you are interested in contributing to this codebase, please contact : freedavep@gmail.com

### License ###

* Dual licensed under a GPLv3 and Commercial license.  See LICENSE.txt for details.

### Extending ClickFic ###

You can extend and customize ClickFic to produce your own mobile applications to publish on the Google Play store (or other venues).  Before starting a project, please take in account the two licensing terms listed in LICENSE.txt and make a decision by the time you are ready to publish.  If you are concerned with copyright, ownership of the code, and selling your application, I recommend [purchasing a commercial license](https://www.clickfic.com/?page=source).

Most of the customization points of ClickFic are found in the **"core"** submodule of this repo.  ClickFic "core" contains the shared LibGDX codebase for the interface, that can be used in both the Android and desktop PC applications (and, eventually, iOS).  Here, you can modify all aspects of the user interface, including look and feel, branding, and general appearance.  The *com.dodacahelix.clickfic.config* package contains the basic configuration in code form.  The core submodule extends from two external libraries --  [IF-engine](https://bitbucket.org/freedave/ddx-ifengine) and [GDX-template](https://bitbucket.org/freedave/ddx-gdx-template).  These are libraries that are managed in separate Git repositories, and licensed under the permissive Apache v2 license.  Feel free to fork and modify these any way that you wish.   Here is an overview of the three repos:

* [IF-engine](https://bitbucket.org/freedave/ddx-ifengine) : A java-based API that maintains the data model for the interactive fiction elements (i.e; environment, persons, narrators, locations, etc..) and the background engine which processes "turns" (what happens to the model when a IF command is made).

* [GDX-template](https://bitbucket.org/freedave/ddx-gdx-template) : A model-view-controller framework for the LibGDX-based frontend.  This is an experimental project for enforcing separation of concerns in a generic LibGDX app.  You most likely will not have to modify this library, unless you are going to make a deep customization of the interface (in which case, you might decide to scrap it entirely). 

* ClickFic core : Sub-module located in this repository, this is an extension of GDX-template, which contains the interface specific to the ClickFic application -- the main menu screen, the reader console and the option tree.  Here you can customize the look and feel of the mobile app, and apply your branding.
or
In the *assets* directory of this Git repo you can find the graphic and font resources used by clickfic-core, as well as the story file(s) themselves (within the *data* sub-directory).


### Building the APK ###

To deploy and run your customized mobile app, you will need to package the application into an Android Application Package (APK) file.  The APK can be created either in *debug* mode, for testing on an emulator or attached Android device, or *release* mode for distribution via the Google Play store.  The release APK will need to be signed with a PKI certificate and zipaligned before it can be uploaded.  You can create the APK using your IDE (such as IntelliJ, Android Studio or Eclipse) or via the command line using the Gradle build tool.  This guide will explain the command line Gradle build.

* Before anything else, build and install the if-engine and gdx-template projects (see above), so that those artifacts are in your local maven repository.  This will involve cloning [gdx-template](https://bitbucket.org/freedave/ddx-gdx-template) and [if-engine](https://bitbucket.org/freedave/ddx-ifengine) and running the *"mvn install"* command.

```
mkdir c:/docs/develop/repos/clickfic
cd c:/docs/develop/repos/clickfic
git clone git@bitbucket.org:freedave/ddx-ifengine.git
git clone git@bitbucket.org:freedave/ddx-gdx-template.git
cd ddx-ifengine
mvn clean install
cd ../ddx-gdx-template
mvn clean install
```

* Create a self-signed PKI certificate to "sign" your APK.  If you already have one for another Android app, you can reuse it (according to Google, you should *"You should sign all of your apps with the same certificate throughout the expected lifespan of your applications."*  If not, [follow the instructions](http://developer.android.com/tools/publishing/app-signing.html) that Google provides in the Android docs.  These instructions will guide you through generating the cert using your IDE or manually from the command line (see bottom of the page for manual instructions).  If manually creating the cert, please be advised that if using JDK7+, you will need to specify the "SHA1withRSA" signature algorithm and the "RSA" key algorithm.

* Create a "gradle.properties" file in either the .gradle subdirectory of this project, or the GRADLE_USER_HOME directory (if you have this specified).  This property file should contain the location of your keystore file, alias and password(s):
```
RELEASE_STORE_FILE={path to your keystore}
RELEASE_STORE_PASSWORD=*****
RELEASE_KEY_ALIAS=*****
RELEASE_KEY_PASSWORD=*****
```
NOTE: if you don't feel comfortable adding a clear-text password to this property file, I recommend setting a configuration parameter when running the gradle build (i.e; *-PkeystorePassword=foo*)

* Run the following Gradle commands from the root of this project to build and deploy the APK:
    * To build a debug APK : ```gradlew android:assembleDebug```
    * To build a release APK : ```gradlew android:assembleRelease```

* The resultant APK file will be located in the *android/build/outputs/apk*
