/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.system;

import java.util.List;

import com.dodecahelix.libgdx.template.text.ListText;
import com.dodecahelix.libgdx.template.text.TextLine;
import com.dodecahelix.libgdx.template.text.TextStyleAlign;
import com.dodecahelix.libgdx.template.text.TextStyleDecor;
import com.dodecahelix.libgdx.template.text.TextStyleSize;
import com.dodecahelix.libgdx.template.ui.RgbColor;
import com.dodecahelix.libgdx.template.view.screens.CreditsText;

public class ClickFicCredits implements CreditsText {

    private ListText text;

    public ClickFicCredits() {

        text = new ListText(TextStyleAlign.CENTERED, TextStyleSize.MEDIUM, RgbColor.BLACK);

        text.addLine("Developed by David Peters");
        text.addHyperlink(0, "www.clickfic.com", "http://www.clickfic.com/");

        text.addBreak();
        text.addSpecialLine("Third Party Products and Libraries", TextStyleDecor.BOLD, TextStyleSize.LARGE, null);

        text.addHyperlink(0, "LIBGDX graphics engine", "https://libgdx.badlogicgames.com/");
        text.addLine(3, "Developer : Badlogic Games");
        text.addHyperlink(3, "License: Apache v2.0", "https://github.com/libgdx/libgdx/blob/master/LICENSE");

        text.addHyperlink(0, "Android Open Source Project", "https://source.android.com/index.html");
        text.addLine(3, "Developer : Google");
        text.addHyperlink(3, "License: Apache v2.0", "http://www.apache.org/licenses/LICENSE-2.0");

        text.addHyperlink(0, "GSON - library for JSON parsing", "https://github.com/google/gson");
        text.addLine(3, "Developer : Google");
        text.addHyperlink(3, "License: Apache v2.0", "https://github.com/google/gson/blob/master/LICENSE");

        text.addBreak();
        text.addSpecialLine("Fonts", TextStyleDecor.BOLD, TextStyleSize.LARGE, null);

        text.addHyperlink(0, "Arimo", "http://www.fontsquirrel.com/fonts/arimo");
        text.addHyperlink(3, "Designer: Steve Matteson", "http://www.ascenderfonts.com/");
        text.addHyperlink(3, "License: Apache v2.0", "http://www.fontsquirrel.com/license/arimo");

        text.addHyperlink(0, "Neuton", "http://www.fontsquirrel.com/fonts/neuton");
        text.addLine(3, "Designer: Brian Zick");
        text.addHyperlink(3, "License: SIL Open Font License v1.10", "http://www.fontsquirrel.com/license/neuton");

        text.addHyperlink(0, "Rosario", "http://www.fontsquirrel.com/fonts/rosario");
        text.addHyperlink(3, "Designer: Omnibus Type", "http://www.omnibus-type.com/");
        text.addHyperlink(3, "License: SIL Open Font License v1.10", "http://www.fontsquirrel.com/license/rosario");

        text.addHyperlink(0, "Roboto", "http://www.fontsquirrel.com/fonts/roboto");
        text.addLine(3, "Designer: Christian Robertson");
        text.addHyperlink(3, "License: Apache v2.0", "http://www.fontsquirrel.com/license/roboto");

        text.addHyperlink(0, "Fjord", "http://www.fontsquirrel.com/fonts/fjord");
        text.addHyperlink(3, "Designer: Viktoriya Grabowska", "http://www.vikaniesiada.blogspot.com");
        text.addHyperlink(3, "License: SIL Open Font License v1.10", "http://www.fontsquirrel.com/license/fjord");

        text.addHyperlink(0, "Open Sans", "http://www.fontsquirrel.com/fonts/open-sans");
        text.addHyperlink(3, "Designer: Ascender Fonts", "http://www.ascenderfonts.com");
        text.addHyperlink(3, "License: Apache v2.0", "http://www.fontsquirrel.com/license/open-sans");

        text.addHyperlink(0, "Arvo", "http://www.fontsquirrel.com/fonts/arvo");
        text.addHyperlink(3, "Designer: Anton Koovit", "http://anton.korkork.com/");
        text.addHyperlink(3, "License: SIL Open Font License v1.10", "http://www.fontsquirrel.com/license/arvo");

        text.addHyperlink(0, "E.B. Garamond", "http://www.georgduffner.at/ebgaramond/");
        text.addHyperlink(3, "Designer: Georg Duffner", "http://www.georgduffner.at/");
        text.addHyperlink(3, "License: SIL Open Font License v1.10", "http://www.fontsquirrel.com/license/eb-garamond");

        text.addBreak();
        text.addSpecialLine("Icons", TextStyleDecor.BOLD, TextStyleSize.LARGE, null);
        text.addLine("Fatcow Icons");
        text.addHyperlink(3, "http://www.fatcow.com/free-icons", "http://www.fatcow.com/free-icons");
        text.addLine(3, "License : Creative Commons Attribution 3.0 License");

        text.addLine("Fugue Icons");
        text.addHyperlink(3, "Yusuke Kamiyamaneby", "http://p.yusukekamiyamane.com/");
        text.addLine(3, "License : Creative Commons Attribution 3.0 License");

        text.addLine("Silk Icons");
        text.addHyperlink(3, "http://www.famfamfam.com/lab/icons/silk/", "http://www.famfamfam.com/lab/icons/silk/");
        text.addLine(3, "License : Creative Commons Attribution 3.0 License");

        text.addBreak();
        text.addLine("The third party tools and products listed above are in no way affiliated with the ClickFic application.");
    }

    @Override
    public List<TextLine> getLines() {
        return text.getLines();
    }

    @Override
    public TextStyleAlign getDefaultAlign() {
        return text.getDefaultAlign();
    }

}
