/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.system;

import java.util.List;

import com.dodecahelix.clickfic.PlatformDAO;
import com.dodecahelix.clickfic.PlatformException;

import com.dodecahelix.ifengine.IfEngine;
import com.dodecahelix.ifengine.choice.ChoiceProcessor;
import com.dodecahelix.ifengine.choice.ChoiceRecorder;
import com.dodecahelix.ifengine.data.save.EnvironmentSave;
import com.dodecahelix.ifengine.response.ResponseType;
import com.dodecahelix.ifengine.response.StyledResponseLine;
import com.dodecahelix.ifengine.environment.EnvironmentProcessor;
import com.dodecahelix.ifengine.choice.ChoiceRecord;
import com.dodecahelix.ifengine.choice.ChoiceResult;

/**
 *   Handles the "system" commands from the options tree
 *
 */
public class SystemCommandHandler {

    private ChoiceRecorder choiceRecorder;
    private ChoiceProcessor choiceProcessor;
    private EnvironmentProcessor environmentProcessor;

    /**
     *   Handles operations whose implementation depends on the platform (i.e; Android or PC)
     */
    private PlatformDAO platform;

    public SystemCommandHandler(PlatformDAO platformDAO, IfEngine ifEngine) {
        this.platform = platformDAO;

        this.choiceRecorder = ifEngine.getChoiceRecorder();
        this.choiceProcessor = ifEngine.getChoiceProcessor();
        this.environmentProcessor = ifEngine.getEnvironmentProcessor();
    }

    public ChoiceResult handleSystemCommand(SystemCommand command) {

        ChoiceResult choiceResult = null;
        switch (command) {
            case EXIT:
                choiceResult = handleExitCommand();
                break;
            case SAVE:
                choiceResult = handleSaveCommand();
                break;
            case LOAD:
                choiceResult = handleLoadCommand();
                break;
            case RESTART:
                choiceResult = handleRestartCommand();
                break;

            // debug commands
            case PRINT_RECORD:
                choiceResult = handlePrintRecord();
                break;
            case START_RECORD:
                choiceResult = handleStartRecord();
                break;
            case SAVE_RECORD:
                choiceResult = handleSaveRecord();
                break;
            case REPLAY_RECORD:
                choiceResult = handleReplayRecord();
                break;
        }

        if (choiceResult == null) {
            choiceResult = new ChoiceResult();
            choiceResult.setCommandDisplay("...");
        }

        return choiceResult;
    }

    private ChoiceResult handleStartRecord() {
        choiceRecorder.resetRecord();

        ChoiceResult result = new ChoiceResult();
        result.setCommandDisplay("Start record...");
        result.addResponseLine(new StyledResponseLine(ResponseType.SYSTEM, "New record started."));
        return result;
    }

    private ChoiceResult handlePrintRecord() {
        choiceRecorder.printRecord();

        ChoiceResult result = new ChoiceResult();
        result.setCommandDisplay("Print record...");
        result.addResponseLine(new StyledResponseLine(ResponseType.SYSTEM, "Record printed (see log file)."));
        return result;
    }


    private ChoiceResult handleReplayRecord() {

        ChoiceResult result = new ChoiceResult();
        result.setCommandDisplay("Replay record...");

        // load the save game from the OS
        List<ChoiceRecord> record = null;
        try {
            record = platform.getChoiceRecord();
        } catch (PlatformException e) {
            result.addResponseLine(new StyledResponseLine(ResponseType.SYSTEM, "Error loading turn record - please review logs for cause."));
        }

        if (record == null) {
            // no save gave data.  Send a message that the game has not been restored
            result.addResponseLine(new StyledResponseLine(ResponseType.SYSTEM, "No stored turn record found."));
        } else {
            try {
                return choiceProcessor.replayChoices(record);
            } catch (Exception e) {
                result.addResponseLine(new StyledResponseLine(ResponseType.SYSTEM, "Error in replay of turn record.  Please review logs for cause."));
            }
        }

        return result;
    }

    private ChoiceResult handleSaveRecord() {

        // gets the diff data for saving
        List<ChoiceRecord> record = choiceRecorder.getRecord();

        // save to the system
        try {
            platform.saveChoiceRecord(record);
        } catch (PlatformException e) {
            throw new IllegalStateException(e);
        }

        ChoiceResult result = new ChoiceResult();
        result.setCommandDisplay("Save record...");
        result.addResponseLine(new StyledResponseLine(ResponseType.SYSTEM, "Record saved."));
        return result;
    }

    private ChoiceResult handleRestartCommand() {
        return environmentProcessor.restartStory();
    }

    private ChoiceResult handleLoadCommand() {

        // load the save game from the OS
        EnvironmentSave environmentSave = null;
        String fileName = environmentProcessor.getStoryIdentifier();
        try {
            environmentSave = platform.restoreSavedProgress(fileName);
        } catch (PlatformException e) {
            throw new IllegalStateException(e);
        }

        if (environmentSave == null) {
            // no save gave data.  Send a message that the game has not been restored
            throw new UnsupportedOperationException("No save game exists for file " + fileName);
        } else {
            return environmentProcessor.loadSavedEnvironment(environmentSave);
        }
    }

    private ChoiceResult handleSaveCommand() {

        // gets the diff data for saving
        EnvironmentSave environmentSaveData = environmentProcessor.saveEnvironment();

        // save to the system
        try {
            platform.saveProgress(environmentSaveData);
        } catch (PlatformException e) {
            throw new IllegalStateException(e);
        }

        ChoiceResult result = new ChoiceResult();
        result.setCommandDisplay("Save game...");
        result.addResponseLine(new StyledResponseLine(ResponseType.SYSTEM, "Game saved."));
        return result;
    }

    private ChoiceResult handleExitCommand() {
        // does not Exit - returns to the main menu
        ChoiceResult result = new ChoiceResult();
        result.setCommandDisplay("Exit (return to main menu)...");
        result.addResponseLine(new StyledResponseLine(ResponseType.SYSTEM, "Waiting to exit game..."));

        return result;
    }

}
