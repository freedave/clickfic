/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.system;

/**
 * System commands to be handled from Brainbook codebase
 *
 */
public enum SystemCommand {

    EXIT("exit", "Exit (return to main menu)", false),
    SAVE("save", "Save progress", false),
    LOAD("load", "Restore saved progress", false),
    RESTART("restart", "Restart the game", false),

    START_RECORD("record.start", "Start a record of turns (debugging)", true),
    PRINT_RECORD("record.print", "Print the record of turns (debugging)", true),
    SAVE_RECORD("record.save", "Store the record of turns to a file (debugging)", true),
    REPLAY_RECORD("record.replay", "Replay the stored record (debugging)", true);

    private String id;
    private String label;
    private boolean debug;

    SystemCommand(String id, String label, boolean debug) {
        // append "system." to ID to identity this in the command handler
        this.id = "system." + id;
        this.label = label;
        this.debug = debug;
    }

    public static SystemCommand getByCommandId(String commandId) {
        for (SystemCommand command : SystemCommand.values()) {
            if (command.getCommandId().equals(commandId)) {
                return command;
            }
        }
        throw new IllegalArgumentException("Invalid commandId : " + commandId);
    }

    public String getLabel() {
        return label;
    }

    public String getCommandId() {
        return id;
    }

    public boolean isDebug() {
        return debug;
    }

}
