/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic;

import com.dodecahelix.clickfic.config.IntConstants;
import com.dodecahelix.clickfic.config.StringConstants;
import com.dodecahelix.clickfic.control.ClickFicEntitySynchController;
import com.dodecahelix.clickfic.control.LibraryController;
import com.dodecahelix.clickfic.control.MasterController;
import com.dodecahelix.clickfic.model.BackgroundEntityFactory;
import com.dodecahelix.clickfic.model.BackgroundGridSpace;
import com.dodecahelix.clickfic.model.ClickFicModel;
import com.dodecahelix.clickfic.model.book.BookSpace;
import com.dodecahelix.clickfic.model.book.BookSpaceEntityFactory;
import com.dodecahelix.clickfic.model.book.IfCommandHandler;
import com.dodecahelix.clickfic.model.book.Library;
import com.dodecahelix.clickfic.model.options.OptionTreeResponseBuilder;
import com.dodecahelix.clickfic.screens.ClickFicLibraryScreen;
import com.dodecahelix.clickfic.screens.ClickFicMainMenuScreen;
import com.dodecahelix.clickfic.screens.ClickFicOptionsScreen;
import com.dodecahelix.clickfic.system.ClickFicCredits;
import com.dodecahelix.clickfic.system.SystemCommandHandler;
import com.dodecahelix.clickfic.view.ClickFicEntityActorFactory;
import com.dodecahelix.clickfic.view.ClickFicScreenType;
import com.dodecahelix.clickfic.view.BookView;
import com.dodecahelix.ifengine.DefaultIfEngine;
import com.dodecahelix.ifengine.IfEngine;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.control.BumpController;
import com.dodecahelix.libgdx.template.control.ControllerMap;
import com.dodecahelix.libgdx.template.control.EntitySynchController;
import com.dodecahelix.libgdx.template.control.ModelController;
import com.dodecahelix.libgdx.template.control.ScreenController;
import com.dodecahelix.libgdx.template.control.SpaceController;
import com.dodecahelix.libgdx.template.control.StageController;
import com.dodecahelix.libgdx.template.control.events.ViewEventController;
import com.dodecahelix.libgdx.template.math.twodim.collision.EntityCollisionAccessor;
import com.dodecahelix.libgdx.template.model.Model;
import com.dodecahelix.libgdx.template.model.access.bump.BumpAccessorMap;
import com.dodecahelix.libgdx.template.model.access.bump.ChessboardBumpAccessor;
import com.dodecahelix.libgdx.template.model.access.bump.DimensionlessBumpAccessor;
import com.dodecahelix.libgdx.template.model.access.bump.GridBumpAccessor;
import com.dodecahelix.libgdx.template.model.access.bump.SidescrollerBumpAccessor;
import com.dodecahelix.libgdx.template.model.access.bump.TilemapBumpAccessor;
import com.dodecahelix.libgdx.template.model.access.bump.UniverseBumpAccessor;
import com.dodecahelix.libgdx.template.model.access.bump.UserInterfaceBumpAccessor;
import com.dodecahelix.libgdx.template.model.access.impl.EntityDeletionAccessor;
import com.dodecahelix.libgdx.template.model.access.impl.EntityLifecycleAccessor;
import com.dodecahelix.libgdx.template.model.access.impl.EntityMotionAccessor;
import com.dodecahelix.libgdx.template.model.access.impl.EntityQueryAccessor;
import com.dodecahelix.libgdx.template.model.access.impl.GridBoundsAccessor;
import com.dodecahelix.libgdx.template.model.access.impl.ModelLoadingAccessor;
import com.dodecahelix.libgdx.template.model.access.impl.SpaceEventAccessor;
import com.dodecahelix.libgdx.template.model.access.impl.SpaceStateAccessor;
import com.dodecahelix.libgdx.template.view.ViewMap;
import com.dodecahelix.libgdx.template.view.actors.entity.EntityActorFactory;
import com.dodecahelix.libgdx.template.view.resource.CachedResourceController;
import com.dodecahelix.libgdx.template.view.resource.ResourceController;
import com.dodecahelix.libgdx.template.view.screens.CreditsScreen;
import com.dodecahelix.libgdx.template.view.screens.CreditsText;
import com.dodecahelix.libgdx.template.view.screens.ExitScreen;
import com.dodecahelix.libgdx.template.view.screens.LoadingScreen;
import com.dodecahelix.libgdx.template.view.screens.MainMenuScreen;
import com.dodecahelix.libgdx.template.view.screens.OptionsScreen;
import com.dodecahelix.libgdx.template.view.screens.SplashScreen;

/**
 *   Takes the place of a DI container, wiring up everything through constructor injection
 *
 * @author dpeters
 *
 */
public class ClickFicDiModule {

    private MasterController masterController;
    private Properties properties;
    private PlatformDAO platformDAO;

    private SystemCommandHandler systemCommandHandler;
    private IfCommandHandler commandHandler;
    private IfEngine ifEngine;
    private Library library;

    private ControllerMap controllers;
    private ViewMap views;
    private BookView bookView;

    // Model
    private Model model;
    private BookSpaceEntityFactory bookSpaceEntityFactory;
    private OptionTreeResponseBuilder optionTreeBuilder;
    private BookSpace bookSpace;
    private BackgroundEntityFactory backgroundEntityFactory;
    private BackgroundGridSpace backgroundGridSpace;

    // Model accessors
    private ModelLoadingAccessor modelLoadingAccessor;
    private EntityLifecycleAccessor entityLifecycleAccessor;
    private BumpAccessorMap bumpAccessorMap;
    private GridBumpAccessor gridBumpAccessor;
    private UniverseBumpAccessor universeBumpAccessor;
    private ChessboardBumpAccessor chessboardBumpAccessor;
    private SidescrollerBumpAccessor sidescrollerBumpAccessor;
    private TilemapBumpAccessor tilemapBumpAccessor;
    private UserInterfaceBumpAccessor userInterfaceBumpAccessor;
    private DimensionlessBumpAccessor dimensionlessBumpAccessor;
    private EntityMotionAccessor entityMotionAccessor;
    private EntityCollisionAccessor entityCollisionAccessor;
    private EntityDeletionAccessor entityDeletionAccessor;
    private EntityQueryAccessor entityQueryAccessor;
    private SpaceEventAccessor spaceEventAccessor;
    private GridBoundsAccessor gridBoundsAccessor;
    private SpaceStateAccessor spaceStateAccessor;

    // Controllers
    private ClickFicPlatformController platformController;
    private LibraryController libraryController;
    private ResourceController resourceController;
    private ModelController modelController;
    private ScreenController screenController;
    private BumpController bumpController;
    private StageController stageController;
    private ViewEventController viewEventController;
    private EntitySynchController entitySynchController;
    private SpaceController spaceController;

    private EntityActorFactory entityActorFactory;

    public ClickFicDiModule(PlatformDAO platformDao) {
        this.platformDAO = platformDao;

        wireDependencies();
    }

    private void wireDependencies() {
        properties = new Properties();

        ifEngine = new DefaultIfEngine();
        library = new Library(platformDAO, ifEngine.getEnvironmentProcessor());

        // wire up the interfaces to the if engine and platform
        systemCommandHandler = new SystemCommandHandler(platformDAO, ifEngine);
        optionTreeBuilder = new OptionTreeResponseBuilder(properties);
        commandHandler = new IfCommandHandler(ifEngine, optionTreeBuilder, systemCommandHandler);

        // wire up the model
        bookSpaceEntityFactory = new BookSpaceEntityFactory(properties);
        bookSpace = new BookSpace(bookSpaceEntityFactory, commandHandler, optionTreeBuilder, properties);
        backgroundEntityFactory = new BackgroundEntityFactory(properties);
        backgroundGridSpace = new BackgroundGridSpace(backgroundEntityFactory, properties);
        model = new ClickFicModel(bookSpace, backgroundGridSpace);

        // wire up the model accessors
        entityDeletionAccessor = new EntityDeletionAccessor(model);
        entityMotionAccessor = new EntityMotionAccessor(model, entityDeletionAccessor);
        entityCollisionAccessor = new EntityCollisionAccessor(model, properties);
        gridBumpAccessor = new GridBumpAccessor(model, entityMotionAccessor, entityCollisionAccessor);
        universeBumpAccessor = new UniverseBumpAccessor(model);
        chessboardBumpAccessor = new ChessboardBumpAccessor(model);
        sidescrollerBumpAccessor = new SidescrollerBumpAccessor(model);
        tilemapBumpAccessor = new TilemapBumpAccessor(model);
        userInterfaceBumpAccessor = new UserInterfaceBumpAccessor(model);
        dimensionlessBumpAccessor = new DimensionlessBumpAccessor(model);
        modelLoadingAccessor = new ModelLoadingAccessor(model);
        entityLifecycleAccessor = new EntityLifecycleAccessor(model, properties);
        entityQueryAccessor = new EntityQueryAccessor(model, properties);
        spaceEventAccessor = new SpaceEventAccessor(model);
        gridBoundsAccessor = new GridBoundsAccessor(model);
        spaceStateAccessor = new SpaceStateAccessor(model);

        // accessor maps
        bumpAccessorMap = new BumpAccessorMap(gridBumpAccessor, universeBumpAccessor, chessboardBumpAccessor, sidescrollerBumpAccessor, tilemapBumpAccessor, userInterfaceBumpAccessor, dimensionlessBumpAccessor);

        //readWriteAccessorMap = new ReadWriteAccessorMap(modelLoadingAccessor, entityQueryAccessor, entityMotionAccessor, entityDeletionAccessor, entityCollisionAccessor, spaceStateAccessor, entityLifecycleAccessor, spaceEventAccessor);
        //readOnlyAccessorMap = new ReadOnlyAccessorMap(gridBoundsAccessor);
        //accessors = new AccessorMap(readWriteAccessorMap, readOnlyAccessorMap, bumpAccessorMap);

        // wire up the controllers
        platformController = new ClickFicPlatformController(platformDAO);
        resourceController = new CachedResourceController();
        modelController = new ModelController(modelLoadingAccessor);
        screenController = new ScreenController(entityLifecycleAccessor);
        bumpController = new BumpController(bumpAccessorMap);
        viewEventController = new ViewEventController(spaceEventAccessor);
        entityActorFactory = new ClickFicEntityActorFactory(resourceController, viewEventController, screenController, properties);
        stageController = new StageController(entityQueryAccessor, entityDeletionAccessor, entityLifecycleAccessor, entityActorFactory, properties);
        entitySynchController = new ClickFicEntitySynchController(entityQueryAccessor, gridBoundsAccessor);
        spaceController = new SpaceController(spaceStateAccessor);
        controllers = new ControllerMap(resourceController, modelController, screenController, bumpController, stageController, entitySynchController, spaceController, viewEventController, platformController);

        // add in any application controllers
        libraryController = new LibraryController(library);
        controllers.addApplicationController(StringConstants.LIBRARY_CONTROLLER_KEY.getValue(), libraryController);

        // wire up the screens
        MainMenuScreen mainMenuScreen = new ClickFicMainMenuScreen(controllers, properties, ClickFicScreenType.APP_SCREEN);
        LoadingScreen loadingScreen = new LoadingScreen(controllers, properties);
        CreditsText creditsText = new ClickFicCredits();
        CreditsScreen creditsScreen = new CreditsScreen(controllers, properties, creditsText);
        SplashScreen splashScreen = new SplashScreen(controllers, properties);
        OptionsScreen optionsScreen = new ClickFicOptionsScreen(controllers, properties);
        ExitScreen exitScreen = new ExitScreen(controllers, properties);
        ClickFicLibraryScreen libraryScreen = new ClickFicLibraryScreen(controllers, properties);

        views = new ViewMap(mainMenuScreen, loadingScreen, creditsScreen, splashScreen, optionsScreen, exitScreen);
        views.add(IntConstants.LIBRARY_SCREEN_ID.getValue(), libraryScreen);

        bookView = new BookView(controllers, properties);

        // finally, wire up the master controller!
        masterController = new MasterController(controllers, model, views, bookView, libraryController);
    }

    public MasterController getMasterController() {
        return masterController;
    }

    public Properties getApplicationProperties() {
        return properties;
    }

    public SystemCommandHandler getSystemCommandHandler() {
        return systemCommandHandler;
    }

    public ClickFicPlatformController getPlatformController() {
        return platformController;
    }

    public IfEngine getIfEngine() { return ifEngine; }
}
