/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.screens;

import java.text.DecimalFormat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane.ScrollPaneStyle;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener.ChangeEvent;
import com.dodecahelix.clickfic.ClickFicPlatformController;
import com.dodecahelix.clickfic.config.ClickFicPropertyName;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.PropertyName;
import com.dodecahelix.libgdx.template.config.SharedConstants;
import com.dodecahelix.libgdx.template.config.UserPreferences;
import com.dodecahelix.libgdx.template.control.ControllerMap;
import com.dodecahelix.libgdx.template.math.twodim.Bounds;
import com.dodecahelix.libgdx.template.text.TextLine;
import com.dodecahelix.libgdx.template.ui.RgbColor;
import com.dodecahelix.libgdx.template.ui.SkinClass;
import com.dodecahelix.libgdx.template.ui.StyledTextTable;
import com.dodecahelix.libgdx.template.util.ScalingUtil;
import com.dodecahelix.libgdx.template.view.resource.FontStyle;
import com.dodecahelix.libgdx.template.view.resource.ResourceController;
import com.dodecahelix.libgdx.template.view.screens.OptionsScreen;

public class ClickFicOptionsScreen extends OptionsScreen {

    private Table contentTable;
    private Skin skin;
    private float padding;

    private SelectBox<Integer> fontSizeSelector;
    private SelectBox<String> fontStyleSelector;

    private RgbColor systemFontColor;
    private RgbColor readingFontColor;
    private RgbColor actionFontColor;
    private RgbColor dialogFontColor;

    private StyledTextTable demoTableContent;

    /**
     *  Format the values with a standard number of digits, so the row doesn't move around
     */
    DecimalFormat decimalFormatter = new DecimalFormat("00.0");

    public ClickFicOptionsScreen(ControllerMap controllers, Properties options) {
        super(controllers, options);
    }

    @Override
    public Actor buildContentActor() {
        contentTable = new Table();

        ResourceController resources = controllers.getResourceController();
        skin = resources.getSkin();
        padding = properties.getScaledFloatProperty(PropertyName.UI_STANDARD_PADDING.name());

        buildDemoTable();

        buildFontSizeSelectorRow();
        buildFontStyleDropdown();

        return contentTable;
    }

    /**
     *   Show a table which displays the changes as a results of the options
     */
    private void buildDemoTable() {
        Bounds screenBounds = controllers.getScreenController().getScreenBounds();
        ScrollPane scroller = new ScrollPane(buildDemoTableContent(), skin);
        scroller.setFadeScrollBars(false);
        scroller.setVariableSizeKnobs(false);

        // erase the default scrollpane background
        ScrollPaneStyle style = new ScrollPaneStyle(scroller.getStyle());
        //style.background = null;
        scroller.setStyle(style);

        // align the scroller
        contentTable.add(scroller).colspan(3).center()
        .width(screenBounds.getWidth() / 1.5f)
        .height(screenBounds.getHeight() / 2.25f)
        .padBottom(padding * 2);
        contentTable.row();
    }

    private Actor buildDemoTableContent() {

        String systemFontColorString = properties.getStringProperty(ClickFicPropertyName.CONSOLE_FONT_COLOR_SYSTEM.name());
        String dialogFontColorString = properties.getStringProperty(ClickFicPropertyName.CONSOLE_FONT_COLOR_DIALOG.name());
        String actionFontColorString = properties.getStringProperty(ClickFicPropertyName.CONSOLE_FONT_COLOR_ACTION.name());
        String readingFontColorString = properties.getStringProperty(ClickFicPropertyName.CONSOLE_FONT_COLOR_READING.name());

        systemFontColor = RgbColor.valueOf(systemFontColorString);
        dialogFontColor = RgbColor.valueOf(dialogFontColorString);
        actionFontColor = RgbColor.valueOf(actionFontColorString);
        readingFontColor = RgbColor.valueOf(readingFontColorString);

        String consoleFontStyleName = properties.getStringProperty(ClickFicPropertyName.CLICKFIC_CONSOLE_FONT.name());
        FontStyle consoleFontStyle = FontStyle.getByName(consoleFontStyleName);
        demoTableContent = new StyledTextTable(controllers.getResourceController(), properties, consoleFontStyle);

        printDemoTableLines();

        return demoTableContent;
    }

    private void printDemoTableLines() {

        TextLine newLine = new TextLine("This is a normal line for reading", null, null, null, readingFontColor);
        demoTableContent.addTextLine(newLine);
        newLine = new TextLine("This is a line for dialog", null, null, null, dialogFontColor);
        demoTableContent.addTextLine(newLine);
        newLine = new TextLine("This is an action line", null, null, null, actionFontColor);
        demoTableContent.addTextLine(newLine);
        newLine = new TextLine("This is a line for system messages", null, null, null, systemFontColor);
        demoTableContent.addTextLine(newLine);
    }

    private void buildFontSizeSelectorRow() {
        // font size label
        Label fontSizeLabel = new Label("Font Size : ", skin.get(SkinClass.LARGE_LABEL.name(), LabelStyle.class));
        float labelWidth = ScalingUtil.scaleFloat(100);
        contentTable.add(fontSizeLabel).width(labelWidth).padRight(padding).spaceBottom(padding);

        // TODO - switch to closest font size??
        int fontSize = this.properties.getScaledIntProperty(PropertyName.UI_STANDARD_FONT_SIZE.name());

        fontSizeSelector = new SelectBox<Integer>(skin);
        fontSizeSelector.getScrollPane().setVariableSizeKnobs(false);

        Integer[] fontSizes = new Integer[]{fontSize, 8, 12, 16, 20, 24, 28, 32, 36, 40, 44, 48, 52, 56, 60};
        fontSizeSelector.setItems(fontSizes);
        fontSizeSelector.setSelected(fontSize);

        fontSizeSelector.addListener(new EventListener() {
            @Override
            public boolean handle(Event event) {
                if (event instanceof ChangeEvent) {
                    Integer newFontSize = fontSizeSelector.getSelected();

                    // set the scaled value
                    properties.addScaledIntegerProperty(PropertyName.UI_STANDARD_FONT_SIZE.name(), (int) newFontSize);

                    // also scale up the option button font (slightly larger)
                    float scalingFactor = ScalingUtil.getScalingFactor();
                    int multiplier = Math.round(scalingFactor / 0.5f);
                    properties.addScaledIntegerProperty(ClickFicPropertyName.OPTION_BUTTON_FONT_SIZE.name(), newFontSize + multiplier * 2);

                    // reset sizes
                    demoTableContent.clearText();
                    demoTableContent.initFonts();
                    printDemoTableLines();
                }
                return false;
            }
        });

        contentTable.add(fontSizeSelector).fillX().left().spaceBottom(padding);
        contentTable.row();
    }

    private void buildFontStyleDropdown() {
        // font size label
        Label fontStyleLabel = new Label("Font Style : ", skin.get(SkinClass.LARGE_LABEL.name(), LabelStyle.class));
        float labelWidth = ScalingUtil.scaleFloat(100);
        contentTable.add(fontStyleLabel).width(labelWidth).padRight(padding);

        String consoleFontStyleName = properties.getStringProperty(ClickFicPropertyName.CLICKFIC_CONSOLE_FONT.name());

        fontStyleSelector = new SelectBox<String>(skin);
        String[] fontDisplayValues = new String[FontStyle.fontStyles.length];

        for (int i = 0; i < FontStyle.fontStyles.length; i++) {
            fontDisplayValues[i] = FontStyle.fontStyles[i].getDisplayName();
        }

        fontStyleSelector.setItems(fontDisplayValues);
        fontStyleSelector.setSelected(consoleFontStyleName);
        fontStyleSelector.getScrollPane().setVariableSizeKnobs(false);

        fontStyleSelector.addListener(new EventListener() {
            @Override
            public boolean handle(Event event) {
                if (event instanceof ChangeEvent) {
                    String newFontStyle = fontStyleSelector.getSelected();
                    properties.addStringProperty(ClickFicPropertyName.CLICKFIC_CONSOLE_FONT.name(), newFontStyle);

                    // reset demo table
                    demoTableContent.clearText();
                    demoTableContent.updateFont(FontStyle.getByName(newFontStyle));
                    printDemoTableLines();
                }
                return false;
            }
        });

        contentTable.add(fontStyleSelector).fillX().left();
        contentTable.row();
    }

    @Override
    public void executeDone() {
        UserPreferences updatedPrefs = new UserPreferences();

        // save user preferences
        int newFontSize = fontSizeSelector.getSelected();
        updatedPrefs.getScalableIntegerPreferences().put(PropertyName.UI_STANDARD_FONT_SIZE.name(), newFontSize);

        float scalingFactor = ScalingUtil.getScalingFactor();
        int multiplier = Math.round(scalingFactor / 0.5f);
        updatedPrefs.getScalableIntegerPreferences().put(ClickFicPropertyName.OPTION_BUTTON_FONT_SIZE.name(), newFontSize + multiplier * 2);

        // also scale the Console and OptionsButton properties - this screws up the menu screens on larger tables

        String newFontStyle = fontStyleSelector.getSelected();
        updatedPrefs.getStringPreferences().put(ClickFicPropertyName.CLICKFIC_CONSOLE_FONT.name(), newFontStyle);

        Gdx.app.debug(SharedConstants.LOGGING_TAG_GAME, "Updating user preferences " + updatedPrefs);
        ClickFicPlatformController platformController = (ClickFicPlatformController) this.getControllerMap().getPlatformController();
        platformController.updateUserPreferences(updatedPrefs);
    }

}
