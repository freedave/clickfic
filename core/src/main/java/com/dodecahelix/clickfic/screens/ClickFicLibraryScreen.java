/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.screens;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.dodecahelix.clickfic.config.StringConstants;
import com.dodecahelix.clickfic.control.LibraryController;
import com.dodecahelix.ifengine.data.story.LibraryCard;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.PropertyName;
import com.dodecahelix.libgdx.template.control.ControllerMap;
import com.dodecahelix.libgdx.template.control.ScreenController;
import com.dodecahelix.libgdx.template.ui.RgbColor;
import com.dodecahelix.libgdx.template.ui.SkinClass;
import com.dodecahelix.libgdx.template.view.resource.ResourceController;
import com.dodecahelix.libgdx.template.view.screens.InformationScreen;

public class ClickFicLibraryScreen extends InformationScreen {

    private Table contentTable;
    private Skin skin;

    private float padding;

    private LibraryController libraryController;
    private ResourceController resources;
    private ScreenController screenController;

    private Label currentlySelectedTitleLabel;
    private Label currentlySelectedAuthorLabel;
    private Label currentlySelectedSynopsisLabel;

    /**
     *  Map of available stories.  Key = Story Title.
     */
    private Map<String, LibraryCard> storyMap = new HashMap<String, LibraryCard>();

    private String currentlySelectedStory;

    public ClickFicLibraryScreen(ControllerMap controllers, Properties options) {
        super(controllers, options);

        libraryController = (LibraryController) controllers.getApplicationController(StringConstants.LIBRARY_CONTROLLER_KEY.getValue());
        resources = controllers.getResourceController();
        screenController = controllers.getScreenController();
    }

    @Override
    public void show() {
        skin = resources.getSkin();
        padding = properties.getScaledFloatProperty(PropertyName.UI_STANDARD_PADDING.name());

        super.show();
    }

    @Override
    protected Actor buildHeaderActor(Skin skin) {
        Table headerActor = new Table();

        float screenWidth = screenController.getScreenBounds().getWidth();
        float headerWidth = screenWidth - 4 * padding;

        Label headerLabel = new Label(getScreenTitle(), skin.get(SkinClass.XL_LABEL.name(), LabelStyle.class));

        headerActor.add(headerLabel).width(headerWidth).spaceBottom(padding);
        headerActor.row();

        headerActor.add(buildCurrentStoryPane()).width(headerWidth);

        return headerActor;
    }

    private Table buildCurrentStoryPane() {

        Table currentStoryMetaPane = new Table(skin);

        LabelStyle storyPaneLabelStyle = skin.get(SkinClass.LARGE_LABEL.name(), LabelStyle.class);
        LabelStyle titleLabelStyle = skin.get(SkinClass.XL_LABEL.name(), LabelStyle.class);

        LibraryCard libraryCard = libraryController.getCurrentStoryMetadata();
        currentlySelectedStory = libraryCard.getTitle();

        String titleLabel = libraryCard.getTitle();
        String authorLabel = String.format("by %s", libraryCard.getAuthor());
        //String versionLabel = String.format("version %s", libraryCard.getVersion());

        currentStoryMetaPane.add(new Label("currently selected:", storyPaneLabelStyle)).left().expandX().spaceBottom(padding);
        currentStoryMetaPane.row();

        currentlySelectedTitleLabel = new Label(titleLabel, titleLabelStyle);
        currentStoryMetaPane.add(currentlySelectedTitleLabel).left();
        currentStoryMetaPane.row();

        currentlySelectedAuthorLabel = new Label(authorLabel, storyPaneLabelStyle);
        currentStoryMetaPane.add(currentlySelectedAuthorLabel).left();
        currentStoryMetaPane.row();

        currentlySelectedSynopsisLabel = new Label(libraryCard.getSynopsis(), storyPaneLabelStyle);
        currentlySelectedSynopsisLabel.setWrap(true);
        float screenWidth = screenController.getScreenBounds().getWidth();
        float synopsisWidth = screenWidth - 6 * padding;
        currentStoryMetaPane.add(currentlySelectedSynopsisLabel).width(synopsisWidth).left();

        return currentStoryMetaPane;
    }

    @Override
    public Actor buildContentActor() {
        contentTable = new Table();

        float spaceBetweenSelections = padding * 1.5f;
        float screenWidth = screenController.getScreenBounds().getWidth();
        float buttonWidth = screenWidth - 6 * padding;

        contentTable.add(new Label("Additional Stories:", skin.get(SkinClass.XL_LABEL.name(), LabelStyle.class)))
            .width(buttonWidth)
            .spaceBottom(spaceBetweenSelections);
        contentTable.row();

        List<LibraryCard> allStories = libraryController.getLibraryStories();
        storyMap.clear();
        for (LibraryCard story : allStories) {
            storyMap.put(story.getTitle(), story);

            Actor storySelectionBox = buildStorySelectionBox(story, contentTable);
            contentTable.add(storySelectionBox).width(buttonWidth).spaceBottom(spaceBetweenSelections);
            contentTable.row();
        }

        return contentTable;
    }

    private Actor buildStorySelectionBox(final LibraryCard libraryCard, Table contentTable) {

        Button storyButton = new Button(skin.get(SkinClass.XL_BUTTON.name(), TextButtonStyle.class));

        // color the button
        String menuButtonColorString = properties.getStringProperty(PropertyName.UI_BUTTON_BACKGROUND_COLOR.name());
        RgbColor menuButtonColor = RgbColor.valueOf(menuButtonColorString);
        storyButton.setColor(menuButtonColor.getColor());

        LabelStyle storyPaneLabelStyle = skin.get(SkinClass.LARGE_LABEL.name(), LabelStyle.class);
        LabelStyle titleLabelStyle = skin.get(SkinClass.XL_LABEL.name(), LabelStyle.class);

        String genre = libraryCard.getGenre() != null ? libraryCard.getGenre().name() : "";
        String contentRating = libraryCard.getContentRating() != null ? libraryCard.getContentRating().name() : "";
        String storySize = libraryCard.getStorySize() != null ? libraryCard.getStorySize().name() : "";

        String titleLabel = libraryCard.getTitle();
        String authorLabel = String.format("by %s", libraryCard.getAuthor());
        String versionLabel = String.format("version %s", libraryCard.getVersion());
        String genreLabel = String.format("Genre: %s", genre);
        String contentRatingLabel = String.format("Content Rating: %s", contentRating);
        String sizeLabel = String.format("Size: %s", storySize);

        storyButton.add(new Label(titleLabel, titleLabelStyle)).left().colspan(2).expandX();
        storyButton.row();

        storyButton.add(new Label(authorLabel, storyPaneLabelStyle)).left().colspan(2);
        storyButton.row();

        storyButton.add(new Label(versionLabel, storyPaneLabelStyle)).left().padRight(padding);
        storyButton.add(new Label(genreLabel, storyPaneLabelStyle)).left().padRight(padding);
        storyButton.row();

        storyButton.add(new Label(sizeLabel, storyPaneLabelStyle)).left();
        storyButton.add(new Label(contentRatingLabel, storyPaneLabelStyle)).left();
        storyButton.row();

        storyButton.addListener(new ActorGestureListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);

                currentlySelectedStory = libraryCard.getTitle();
                currentlySelectedTitleLabel.setText(libraryCard.getTitle());
                currentlySelectedAuthorLabel.setText(libraryCard.getAuthor());
                currentlySelectedSynopsisLabel.setText(libraryCard.getSynopsis());
            }
        });

        return storyButton;
    }

    @Override
    public void executeDone() {
        LibraryCard selectedLibraryCard = storyMap.get(currentlySelectedStory);
        libraryController.checkoutNewTitleFromLibrary(selectedLibraryCard);
    }

    @Override
    public String getScreenTitle() {
        return "Library";
    }

}
