/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.dodecahelix.clickfic.config.IntConstants;
import com.dodecahelix.clickfic.config.StringConstants;
import com.dodecahelix.clickfic.control.LibraryController;
import com.dodecahelix.ifengine.data.story.LibraryCard;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.PropertyName;
import com.dodecahelix.libgdx.template.control.ControllerMap;
import com.dodecahelix.libgdx.template.types.ScreenType;
import com.dodecahelix.libgdx.template.types.ScreenType.CommonScreen;
import com.dodecahelix.libgdx.template.ui.SkinClass;
import com.dodecahelix.libgdx.template.view.screens.MainMenuScreen;

public class ClickFicMainMenuScreen extends MainMenuScreen {

    private Skin skin;

    public ClickFicMainMenuScreen(ControllerMap controllers, Properties options, ScreenType mainApplicationScreenType) {
        super(controllers, options, mainApplicationScreenType);
    }

    private void buildScreen() {
        skin = controllers.getResourceController().getSkin();

        // create a table and fill up the screen
        buttonTable = new Table(skin);
        buttonTable.setFillParent(true);
        stage.addActor(buttonTable);

        buildHeader();
        buildCurrentStoryPane();
        buildMenu();
    }

    private void buildHeader() {
        float spacing = properties.getScaledFloatProperty(PropertyName.UI_STANDARD_PADDING.name()) * 2;

        String welcomeMessage = properties.getStringProperty(PropertyName.WELCOME_MESSAGE.name());
        buttonTable.add(new Label(welcomeMessage, skin.get(SkinClass.XL_LABEL.name(), LabelStyle.class))).spaceBottom(spacing);
        buttonTable.row();
    }

    private void buildCurrentStoryPane() {
        Table currentStoryPane = new Table(skin);

        LabelStyle storyPaneLabelStyle = skin.get(SkinClass.MEDIUM_LABEL.name(), LabelStyle.class);
        LabelStyle titleLabelStyle = skin.get(SkinClass.LARGE_LABEL.name(), LabelStyle.class);

        LibraryController libraryController = (LibraryController) controllers
            .getApplicationController(StringConstants.LIBRARY_CONTROLLER_KEY.getValue());
        LibraryCard libraryCard = libraryController.getCurrentStoryMetadata();

        String titleLabel = libraryCard.getTitle();
        String authorLabel = String.format("by %s", libraryCard.getAuthor());
        String versionLabel = String.format("version %s", libraryCard.getVersion());

        currentStoryPane.add(new Label("currently reading:", storyPaneLabelStyle)).left();
        currentStoryPane.row();

        currentStoryPane.add(new Label(titleLabel, titleLabelStyle)).left();
        currentStoryPane.row();

        currentStoryPane.add(new Label(authorLabel, storyPaneLabelStyle)).left();
        currentStoryPane.row();

        currentStoryPane.add(new Label(versionLabel, storyPaneLabelStyle)).left();
        currentStoryPane.row();

        float spacing = properties.getScaledFloatProperty(PropertyName.UI_STANDARD_PADDING.name()) * 2;
        buttonTable.add(currentStoryPane).spaceBottom(spacing);
        buttonTable.row();
    }

    @Override
    public void buildMenu() {
        buildMenuButtons();
    }

    public void buildMenuButtons() {
        buildMenuButton("Start", mainApplicationScreenType.getCode());

        buildMenuButton("More Stories..", IntConstants.LIBRARY_SCREEN_ID.getValue());
        buildMenuButton("Options", CommonScreen.OPTIONS.getId());
        buildMenuButton("Credits", CommonScreen.CREDITS.getId());
        buildMenuButton("Exit", CommonScreen.EXIT.getId());
    }

    @Override
    public void show() {
        // clear the old actors and rebuild the stage from scratch
        stage.clear();

        this.addTiledBackgroundTexture();
        buildScreen();

        // set the stage as the input processor
        Gdx.input.setInputProcessor(stage);
    }

}
