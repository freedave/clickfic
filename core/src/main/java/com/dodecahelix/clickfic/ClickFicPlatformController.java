/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic;

import java.util.Map.Entry;

import com.badlogic.gdx.Gdx;
import com.dodecahelix.libgdx.template.config.SharedConstants;
import com.dodecahelix.libgdx.template.config.UserPreferences;
import com.dodecahelix.libgdx.template.control.PlatformController;

/**
 *   Provides underlying access to the platform
 *
 * @author dpeters
 *
 */
public class ClickFicPlatformController implements PlatformController {

    private PlatformDAO platformDAO;

    public ClickFicPlatformController(PlatformDAO platformDAO) {
        this.platformDAO = platformDAO;
    }

    public PlatformDAO getPlatformDAO() {
        return platformDAO;
    }

    public void updateUserPreferences(UserPreferences userPreferences) {
        try {
            // load the old values and override
            UserPreferences existingPrefs = platformDAO.loadUserPreferences();

            // merge in the updated properties (instead of overwriting everything)
            //  there could be values updated elsewhere
            for (Entry<String, Boolean> preference : userPreferences.getBooleanPreferences().entrySet()) {
                existingPrefs.getBooleanPreferences().put(preference.getKey(), preference.getValue());
            }
            for (Entry<String, String> preference : userPreferences.getStringPreferences().entrySet()) {
                existingPrefs.getStringPreferences().put(preference.getKey(), preference.getValue());
            }
            for (Entry<String, Integer> preference : userPreferences.getIntegerPreferences().entrySet()) {
                existingPrefs.getIntegerPreferences().put(preference.getKey(), preference.getValue());
            }
            for (Entry<String, Float> preference : userPreferences.getFloatPreferences().entrySet()) {
                existingPrefs.getFloatPreferences().put(preference.getKey(), preference.getValue());
            }

            platformDAO.saveUserPreferences(userPreferences);
        } catch (PlatformException e) {
            // can't save user preferences?  Just print the error and continue
            e.printStackTrace();
            Gdx.app.error(SharedConstants.LOGGING_TAG_APPLICATION, "user preferences could not be updated.");

        }
    }

}
