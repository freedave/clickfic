/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.view;

import java.util.HashSet;
import java.util.Set;

import com.dodecahelix.clickfic.model.ClickFicModel;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.control.ControllerMap;
import com.dodecahelix.libgdx.template.model.SpaceId;
import com.dodecahelix.libgdx.template.view.View;

/**
 *   Passes screen events to the controller (show, hide, pause, resume, dispose)
 *
 *   @author dpeters
 *
 */
public class BookView extends View {

    private Set<SpaceId> associatedSpaces;

    public BookView(ControllerMap controllers, Properties options) {
        super(controllers, options);

        associatedSpaces = new HashSet<SpaceId>();
        associatedSpaces.add(ClickFicModel.spaceMap.get(ClickFicModel.BACKGROUND_ID));
        associatedSpaces.add(ClickFicModel.spaceMap.get(ClickFicModel.BOOK_ID));
    }

    @Override
    public Set<SpaceId> getAssociatedSpaces() {
        return associatedSpaces;
    }

}
