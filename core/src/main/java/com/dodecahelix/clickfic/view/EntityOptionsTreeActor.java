/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.view;

import java.util.LinkedList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.dodecahelix.clickfic.config.ClickFicPropertyName;
import com.dodecahelix.clickfic.config.IconKeys;
import com.dodecahelix.clickfic.model.ClickFicAttribute;
import com.dodecahelix.clickfic.model.options.Option;
import com.dodecahelix.clickfic.model.options.OptionsTreeEntity;
import com.dodecahelix.clickfic.system.SystemCommand;
import com.dodecahelix.ifengine.util.StringUtils;
import com.dodecahelix.libgdx.template.config.PropertyName;
import com.dodecahelix.libgdx.template.config.SharedConstants;
import com.dodecahelix.libgdx.template.control.ScreenController;
import com.dodecahelix.libgdx.template.control.events.StandardViewEventType;
import com.dodecahelix.libgdx.template.control.events.ViewEvent;
import com.dodecahelix.libgdx.template.types.ScreenType.CommonScreen;
import com.dodecahelix.libgdx.template.ui.RgbColor;
import com.dodecahelix.libgdx.template.ui.SkinClass;
import com.dodecahelix.libgdx.template.util.ScalingUtil;
import com.dodecahelix.libgdx.template.view.actors.components.IconTextButton;
import com.dodecahelix.libgdx.template.view.actors.components.IconTextButton.IconTextButtonStyle;
import com.dodecahelix.libgdx.template.view.actors.entity.EntityDialogActor;
import com.dodecahelix.libgdx.template.view.resource.FontStyle;
import com.dodecahelix.libgdx.template.view.resource.IconKey;

/**
 *   Panel for displaying a tree list of options
 *
 * @author dpeters
 *
 */
public class EntityOptionsTreeActor extends EntityDialogActor {

    private Table optionsPanel;
    private Table buttonTable;
    private ScrollPane buttonScrollPane;

    private IconTextButtonStyle iconTextButtonStyle;
    private TextButtonStyle textButtonStyle;
    private LabelStyle optionsHeaderStyle;

    private Label optionsHeaderLabel;

    // history of the Option objects that have been clicked (starting with the root option)
    private LinkedList<Option> optionTrail = new LinkedList<Option>();

    // has the ability to switch back to the main menu screen
    private ScreenController screenController;

    @Override
    public void build() {
        super.build();
        this.setTransparentFrame(true);

        // load the style of the button (this may have changed)
        String buttonFontStyleName = properties.getStringProperty(ClickFicPropertyName.CLICKFIC_CONSOLE_FONT.name());
        String fontColor = properties.getStringProperty(ClickFicPropertyName.OPTION_BUTTON_FONT_COLOR.name());
        int fontSize = properties.getScaledIntProperty(ClickFicPropertyName.OPTION_BUTTON_FONT_SIZE.name());

        FontStyle buttonFontStyle = FontStyle.getByName(buttonFontStyleName);
        BitmapFont buttonFont = resources.findFont(buttonFontStyle, fontSize);

        textButtonStyle = new TextButtonStyle(resources.getSkin().get(SkinClass.LARGE_BUTTON.name(), TextButtonStyle.class));
        textButtonStyle.fontColor = RgbColor.valueOf(fontColor).getColor();
        textButtonStyle.font = buttonFont;

        iconTextButtonStyle = new IconTextButtonStyle(textButtonStyle);

        optionsHeaderStyle = new LabelStyle(resources.getSkin().get(SkinClass.LARGE_LABEL.name(), LabelStyle.class));
        optionsHeaderStyle.fontColor = RgbColor.valueOf(fontColor).getColor();
        optionsHeaderStyle.font = buttonFont;

        buildOptionsPanel();

        float padding = properties.getScaledFloatProperty(PropertyName.UI_STANDARD_PADDING.name());
        dialog.add(optionsPanel).pad(padding).expand().fill();
    }

    /**
     *   Creates the initial empty options panel
     */
    private void buildOptionsPanel() {
        optionsPanel = new Table();

        optionsHeaderLabel = new Label("Actions", optionsHeaderStyle);

        int fontSize = properties.getScaledIntProperty(ClickFicPropertyName.OPTION_BUTTON_FONT_SIZE.name());

        float bottomPadding = ScalingUtil.scaleFloat(5.0f);
        float headerHeight = fontSize + bottomPadding;

        // fixed height.. issue
        optionsPanel.add(optionsHeaderLabel).height(headerHeight).padBottom(bottomPadding).center();
        optionsPanel.row();

        buttonTable = new Table();
        buttonScrollPane = new ScrollPane(buttonTable, resources.getSkin());
        buttonScrollPane.setFadeScrollBars(false);
        buttonScrollPane.setVariableSizeKnobs(false);

        //optionsPanel.add(buttonScrollPane).expand().fillX();
        optionsPanel.add(buttonScrollPane).expand().fill();
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);

        if (isInitialized()) {
            // set false by the EntitySyncController, once the sync happens
            boolean optionsChanged = !entityClone.isSynched();
            if (optionsChanged) {
                Gdx.app.log(SharedConstants.LOGGING_TAG_VIEW, "Updating options tree");
                optionTrail.clear();

                Option rootOption = new Option("Actions");
                rootOption.setChildOptions(((OptionsTreeEntity) entityClone).getOptions());
                optionTrail.add(rootOption);
                buildButtonTree(rootOption);

                // reset the header
                optionsHeaderLabel.setText("Actions");

                // don't change buttons until model changes again
                entityClone.setSynched(true);
            }
        }
    }

    private void buildButtonTree(Option parentOption) {

        Array<Option> options = parentOption.getChildOptions();
        buttonTable.clear();

        float buttonPadding = ScalingUtil.scaleFloat(5.0f);

        for (Option option : options) {
            Button button = buildOptionButton(option);
            buttonTable.add(button).pad(buttonPadding).expandX().fillX();
            buttonTable.row();
        }

        // add back button (unless this is the root)
        if (!optionTrail.isEmpty() && optionTrail.size() > 1) {
            Button button = buildBackButton();
            buttonTable.add(button).pad(buttonPadding).expandX().fillX();
            buttonTable.row();
        }

        // scroll back to the top
        buttonScrollPane.setScrollY(0);

    }

    private Button buildBackButton() {
        int optionHistorySize = optionTrail.size();
        final Option backOption = optionTrail.get(optionHistorySize - 2);

        TextButton button = new TextButton("Back to " + backOption.getDisplay(), textButtonStyle);
        button.getLabel().setAlignment(Align.left, Align.center);
        button.setColor(RgbColor.RED_CRIMSON.getColor());

        // TODO - make this configurable?
        float innerPadding = ScalingUtil.scaleFloat(10.0f);
        button.pad(innerPadding);

        button.addListener(new ActorGestureListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                Gdx.app.debug(SharedConstants.LOGGING_TAG_VIEW, "clicked back button to " + backOption.getDisplay());
                super.touchUp(event, x, y, pointer, button);

                optionTrail.pollLast();
                buildButtonTree(backOption);
                optionsHeaderLabel.setText(backOption.getDisplay());
            }
        });

        return button;
    }

    private Button buildOptionButton(final Option option) {

        Button button;
        TextureRegion icon = getIconForOption(option);
        if (icon == null) {
            // No icon defined.  Use a regular TextButton
            button = new TextButton(option.getDisplay(), textButtonStyle);
            ((TextButton) button).getLabel().setAlignment(Align.left, Align.center);
        } else {
            // scale the icon, based on screen size - TODO configure
            int scaledIconSize = ScalingUtil.scaleInteger(20);
            button = new IconTextButton(option.getDisplay(), scaledIconSize, iconTextButtonStyle);
            ((IconTextButton) button).getIcon().setDrawable(new TextureRegionDrawable(icon));
        }

        boolean branchNode = option.isGroupOption();
        if (branchNode) {
            button.setColor(RgbColor.YELLOW_KHAKI.getColor());
        } else {
            button.setColor(RgbColor.BLUE_DARKCYAN.getColor());
        }

        float innerPadding = ScalingUtil.scaleFloat(10.0f);
        button.pad(innerPadding);

        button.addListener(new ActorGestureListener() {
            @Override
            /**
             *  This is a tap event, since the user can scroll through the actions via a long-press
             */
            public void tap(InputEvent event, float x, float y, int pointer, int button) {
                Gdx.app.debug(SharedConstants.LOGGING_TAG_VIEW, "click event on command button " + option.getCommand());
                super.touchUp(event, x, y, pointer, button);

                if (option.getChildOptions() != null) {
                    // this is a grouping - navigate into a subgroup
                    optionTrail.add(option);
                    buildButtonTree(option);
                    optionsHeaderLabel.setText(option.getDisplay());
                } else {
                    // this is a command
                    String commandId = option.getCommand();

                    // system command?  if true, do client side handling
                    if (StringUtils.equalsIgnoreCase(SystemCommand.EXIT.getCommandId(), commandId)) {
                        screenController.setScreen(CommonScreen.MAIN_MENU.getId());
                    }

                    ViewEvent action = eventHandler.generateEvent(StandardViewEventType.BUTTON_CLICK.getEventTypeId());
                    action.setStringValue(ClickFicAttribute.AttributeId.COMMAND_ID.getAttributeCode(), commandId);
                }
            }
        });

        return button;
    }

    private TextureRegion getIconForOption(Option option) {
        TextureRegion icon = null;
        String optionType = option.getOptionType();

        if (optionType != null) {
            IconKey iconKey = IconKeys.lookup(optionType);
            if (iconKey != null) {
                icon = resources.findIcon(iconKey);
            }
        }

        return icon;
    }

    public void setScreenController(ScreenController screenController) {
        this.screenController = screenController;
    }

}
