/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.view;

import com.dodecahelix.libgdx.template.types.ScreenType;

/**
 *  Define the different screens of your application (contains default: i.e; Credits screen)
 *
 * @author dpeters
 *
 */
public class ClickFicScreenType extends ScreenType {

    public static final ClickFicScreenType APP_SCREEN = new ClickFicScreenType("app.screen", 100);

    public ClickFicScreenType(String name, int referenceCode) {
        super(name, referenceCode);
    }


}
