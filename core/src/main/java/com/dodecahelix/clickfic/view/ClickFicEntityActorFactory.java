/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.view;

import com.dodecahelix.clickfic.config.ClickFicPropertyName;
import com.dodecahelix.clickfic.config.ClickFicTextureKey;
import com.dodecahelix.clickfic.model.options.OptionsTreeEntity;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.control.ScreenController;
import com.dodecahelix.libgdx.template.control.events.ViewEventController;
import com.dodecahelix.libgdx.template.model.Entity;
import com.dodecahelix.libgdx.template.model.EntityState;
import com.dodecahelix.libgdx.template.model.interfaces.ConsoleEntity;
import com.dodecahelix.libgdx.template.model.interfaces.UiEntity;
import com.dodecahelix.libgdx.template.model.twodim.GridEntity;
import com.dodecahelix.libgdx.template.types.EntityType.CommonEntity;
import com.dodecahelix.libgdx.template.view.actors.entity.EntityActor;
import com.dodecahelix.libgdx.template.view.actors.entity.EntityActorFactory;
import com.dodecahelix.libgdx.template.view.actors.entity.EntityBackgroundTilesActor;
import com.dodecahelix.libgdx.template.view.actors.entity.EntityConsoleActor;
import com.dodecahelix.libgdx.template.view.resource.ResourceController;

/**
 *   Responsible for building actors for the stage
 *
 * @author dpeters
 *
 */
public class ClickFicEntityActorFactory implements EntityActorFactory {

    ResourceController resources;
    ViewEventController eventHandler;
    ScreenController screenController;

    Properties properties;

    public ClickFicEntityActorFactory(ResourceController resources,
                                      ViewEventController eventHandler,
                                      ScreenController screenController,
                                      Properties properties) {

        super();
        this.resources = resources;
        this.eventHandler = eventHandler;
        this.properties = properties;
        this.screenController = screenController;
    }

    public void init() {
        // called during splash screen

        initPools();
    }

    private void initPools() {
        // Label pool?
    }

    public EntityActor<? extends Entity> buildFromEntity(Entity entity) {
        EntityActor<? extends Entity> actor = null;
        int entityTypeId = entity.getEntityType();

        if (entityTypeId == CommonEntity.BACKGROUND_TILES.getId()) {
            actor = buildBackgroundTilesActor(entity);
        } else if (entityTypeId == CommonEntity.CONSOLE.getId()) {
            actor = buildConsoleActor(entity);
        } else if (entityTypeId == CommonEntity.OPTIONS.getId()) {
            actor = buildOptionsActor(entity);
        }

        if (actor != null) {
            actor.initialize(resources, properties, eventHandler);
            actor.build();
        }

        // build the view using resources
        return actor;
    }

    private EntityActor<GridEntity> buildBackgroundTilesActor(Entity entity) {
        EntityBackgroundTilesActor actor = new EntityBackgroundTilesActor(ClickFicTextureKey.TILE_BACKGROUND);

        GridEntity clone = new GridEntity(entity.getSpaceId());
        clone.setId(entity.getId());
        actor.setEntityClone(clone);
        actor.getEntityClone().setup(entity.getEntityType(), EntityState.ACTIVE);
        actor.getEntityClone().getAttributes().copyFrom(entity.getAttributes());

        return actor;
    }

    private EntityActor<UiEntity> buildConsoleActor(Entity entity) {
        EntityConsoleActor actor = new EntityConsoleActor(ClickFicPropertyName.CLICKFIC_CONSOLE_FONT.name());

        ConsoleEntity clone = new ConsoleEntity(entity.getSpaceId());
        clone.setId(entity.getId());
        actor.setEntityClone(clone);
        actor.getEntityClone().setup(entity.getEntityType(), EntityState.ACTIVE);
        actor.getEntityClone().getAttributes().copyFrom(entity.getAttributes());

        // TODO - pass statically typed attributes
        return actor;
    }

    private EntityActor<UiEntity> buildOptionsActor(Entity entity) {
        EntityOptionsTreeActor actor = new EntityOptionsTreeActor();

        OptionsTreeEntity clone = new OptionsTreeEntity(entity.getSpaceId());
        clone.setId(entity.getId());
        actor.setEntityClone(clone);
        actor.getEntityClone().setup(entity.getEntityType(), EntityState.ACTIVE);
        actor.getEntityClone().getAttributes().copyFrom(entity.getAttributes());

        // give the options tree the ability to switch screens (back to Main Menu)
        actor.setScreenController(screenController);

        return actor;
    }

}
