/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.dodecahelix.clickfic.config.ClickFicProperties;
import com.dodecahelix.clickfic.config.ClickFicPropertyName;
import com.dodecahelix.clickfic.config.ClickFicTypes;
import com.dodecahelix.clickfic.config.StringConstants;
import com.dodecahelix.clickfic.control.MasterController;
import com.dodecahelix.ifengine.data.serializer.StoryBundle;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.PropertiesUtil;
import com.dodecahelix.libgdx.template.config.SharedConstants;
import com.dodecahelix.libgdx.template.config.UserPreferences;

public class ClickFic extends Game {

    private MasterController masterController;

    private PlatformDAO platform;

    public ClickFic(PlatformDAO platform) {
        this.platform = platform;
    }

    @Override
    public void create() {
        Gdx.app.setLogLevel(com.badlogic.gdx.Application.LOG_DEBUG);
        Gdx.app.debug(SharedConstants.LOGGING_TAG_LIFECYCLE, "Creating game.");

        injectDependencies();

        masterController.init();
    }

    private void injectDependencies() {
        Gdx.app.log(SharedConstants.LOGGING_TAG_LIFECYCLE, "Initializing DI module");

        // initialize DI module
        ClickFicDiModule diModule = new ClickFicDiModule(platform);
        Gdx.app.log(SharedConstants.LOGGING_TAG_LIFECYCLE, "DI initialized");

        masterController = diModule.getMasterController();
        masterController.setGame(this);

        Gdx.app.log(SharedConstants.LOGGING_TAG_LIFECYCLE, "Loading application properties and user preferences");
        Properties properties = initializeProperties(diModule.getApplicationProperties());

        // Adds the ClickFic specific types (object and attributes) to SharedTypes
        ClickFicTypes.loadTypes();

        // TODO - Do we need to do this on startup?
        loadStory(diModule, properties);

        Gdx.app.log(SharedConstants.LOGGING_TAG_LIFECYCLE, "Application initialized.");
    }

    private Properties initializeProperties(Properties properties) {

        // overwrite template defaults with app-specific values
        ClickFicProperties.overwriteDefaults(properties);

        // overwrite with user preferences
        try {
            UserPreferences userPreferences = platform.loadUserPreferences();
            properties = PropertiesUtil.overwriteWithUserPreferences(properties, userPreferences);
        } catch (PlatformException e) {
            // can't find user preferences - print an error and move on without it..
            e.printStackTrace();
            Gdx.app.error(SharedConstants.LOGGING_TAG_APPLICATION, "user preferences could not be loaded from platform.  no user preferences will be used.");
        }

        // scale UI widths and heights to screen size
        PropertiesUtil.scaleProperties(properties);

        // initialize any properties specific to the platform
        platform.initializeProperties(properties);

        Gdx.app.log(SharedConstants.LOGGING_TAG_APPLICATION, "initialized properties : " + properties.toString());

        return properties;
    }

    /**
     * Loads the current story.
     *
     * This could be either:
     * 1. The story set in user preferences (when you exit ClickFic, it stores the currently-reading story in preferences)
     * 2. The default story (if nothing is found in user preferences).
     *
     * @param diModule
     * @param properties
     */
    private void loadStory(ClickFicDiModule diModule, Properties properties) {
        StoryBundle storyBundle = null;
        try {
            String storyName = properties.getStringProperty(ClickFicPropertyName.CURRENT_STORY.name());
            Gdx.app.debug(StringConstants.LOGGING_TAG_CLICKFIC.getValue(), "Loading current story " + storyName + " into data model.");
            storyBundle = platform.loadStoryFile(storyName);
        } catch (PlatformException e) {
            // if we can't load the story listed in preferences, load the default
            Gdx.app.log(StringConstants.LOGGING_TAG_CLICKFIC.getValue(), "Exception loading the current story.  Loading default story instead.", e);
            storyBundle = platform.loadStoryFile(StringConstants.DEFAULT_STORY.getValue());
        }

        diModule.getIfEngine().getEnvironmentProcessor().loadStory(storyBundle);
        Gdx.app.debug(StringConstants.LOGGING_TAG_CLICKFIC.getValue(), "loaded story file.");
    }

    @Override
    public void dispose() {
        Gdx.app.debug(SharedConstants.LOGGING_TAG_LIFECYCLE, "Disposing app");
        super.dispose();
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void resize(int width, int height) {
        Gdx.app.debug(SharedConstants.LOGGING_TAG_LIFECYCLE, "Resizing app to: " + width + " x " + height);
        super.resize(width, height);
    }

    @Override
    public void pause() {
        Gdx.app.debug(SharedConstants.LOGGING_TAG_LIFECYCLE, "Pausing app");
        super.pause();

        // no need to pause ClickFic, just exit
        if (platform.isMobile()) {
            Gdx.app.debug(SharedConstants.LOGGING_TAG_LIFECYCLE, "Exiting app");
            Gdx.app.exit();
        }
    }

    @Override
    public void resume() {
        Gdx.app.debug(SharedConstants.LOGGING_TAG_LIFECYCLE, "Resuming app");
        super.resume();
    }

}
