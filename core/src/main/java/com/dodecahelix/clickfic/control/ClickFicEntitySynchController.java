/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.control;

import com.dodecahelix.libgdx.template.control.EntitySynchController;
import com.dodecahelix.libgdx.template.model.Entity;
import com.dodecahelix.libgdx.template.model.access.impl.EntityQueryAccessor;
import com.dodecahelix.libgdx.template.model.access.impl.GridBoundsAccessor;
import com.dodecahelix.libgdx.template.view.actors.entity.EntityActor;

public class ClickFicEntitySynchController extends EntitySynchController {

    public ClickFicEntitySynchController(EntityQueryAccessor queryAccessor,
                                         GridBoundsAccessor gridBoundsAccessor) {
        super(queryAccessor, gridBoundsAccessor);
    }

    @Override
    public void synchronizeEntities(EntityActor<? extends Entity> actor, Entity entity) {
    }

}
