/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.control;

import java.util.List;

import com.dodecahelix.clickfic.config.StringConstants;
import com.dodecahelix.clickfic.model.book.Library;
import com.dodecahelix.ifengine.data.story.LibraryCard;
import com.dodecahelix.libgdx.template.control.Controller;

/**
 *
 *   Controls view access to the library of stories.
 *
 *   This controller is used for the MainMenu and LibraryScreen to access the story lists and to load a new environment
 *
 *   This holds a reference to the Library object directly.  Should this go through the "BookSpace"?
 *   ANS: No, BookSpace is a model object for the application screen, not the main menu and library screens.
 *
 * @author dpeters
 *
 */
public class LibraryController implements Controller {

    private Library library;

    public LibraryController(Library library) {
        super();
        this.library = library;
    }

    public void init() {
    }

    @Override
    public String getControllerKey() {
        return StringConstants.LIBRARY_CONTROLLER_KEY.getValue();
    }

    public LibraryCard getCurrentStoryMetadata() {
        // simplified so as not to use accessor
        //accessors.getReadWriteAccessors().getEntityQueryAccessor().findEntityInActiveSpaces(id)
        return library.getCurrentStory();
    }

    /**
     *   List of all stories in the lirary to select from
     *
     * @return
     */
    public List<LibraryCard> getLibraryStories() {

        return library.getAvailableStories();
    }

    public void checkoutNewTitleFromLibrary(LibraryCard selectedLibraryCard) {

        library.checkoutNewTitle(selectedLibraryCard);
    }

}
