/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.control;

import com.badlogic.gdx.Game;
import com.dodecahelix.clickfic.view.ClickFicScreenType;
import com.dodecahelix.clickfic.view.BookView;
import com.dodecahelix.libgdx.template.control.ControllerMap;
import com.dodecahelix.libgdx.template.control.ScreenController;
import com.dodecahelix.libgdx.template.model.Model;
import com.dodecahelix.libgdx.template.types.ScreenType;
import com.dodecahelix.libgdx.template.view.ViewMap;

/**
 *   Master controller for the entire game.  
 *
 *   Maps the various screens to their types.
 *   Maps the various Grid's to each screen
 *   Initializes the grid(s)
 *
 * @author dpeters
 *
 */
public class MasterController {

    private Game game;

    private ControllerMap controllers;

    @SuppressWarnings("unused")
    private Model model;

    private ViewMap views;

    private BookView applicationView;

    /**
     *  Controls the metadata for the currently loaded story for access from the library and main menu screens
     *
     */
    private LibraryController libraryController;

    public MasterController(ControllerMap controllers,
                            Model model,
                            ViewMap views,
                            BookView applicationView,
                            LibraryController libraryController) {

        super();
        this.controllers = controllers;
        this.model = model;
        this.views = views;
        this.applicationView = applicationView;
        this.libraryController = libraryController;
    }

    public void init() {
        // this sets up the initial wiring of the controllers and views (screens)

        // initialize the map containing the views
        ScreenController screenController = controllers.getScreenController();
        screenController.init(game, views);

        // add application-specific views (screens)
        screenController.addView(ClickFicScreenType.APP_SCREEN.getCode(), applicationView);

        // add application-specific controllers
        controllers.addApplicationController(libraryController.getControllerKey(), libraryController);

        // model and resources are not loaded at this point.  Once splash screen is up, these are loaded/initialized
        screenController.setScreen(ScreenType.CommonScreen.SPLASH.getId());
    }

    public void setGame(Game application) {
        this.game = application;
    }

}