/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic;

public class PlatformException extends RuntimeException {

    private static final long serialVersionUID = -5078228461885240147L;

    public PlatformException() {
    }

    public PlatformException(String message) {
        super(message);
    }

    public PlatformException(Throwable throwable) {
        super(throwable);
    }

    public PlatformException(String message, Throwable throwable) {
        super(message, throwable);
    }

}
