/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.config;

import com.dodecahelix.clickfic.model.ClickFicAttribute;
import com.dodecahelix.clickfic.model.ClickFicEntityType;
import com.dodecahelix.clickfic.model.ClickFicAttribute.AttributeId;
import com.dodecahelix.clickfic.model.ClickFicEntityType.ClickFicEntity;
import com.dodecahelix.libgdx.template.config.SharedTypes;

public class ClickFicTypes {

    public static void loadTypes() {
        for (ClickFicEntity entity : ClickFicEntityType.ClickFicEntity.values()) {
            SharedTypes.getInstance().addEntityType(new ClickFicEntityType(entity));
        }
        for (AttributeId attribute : ClickFicAttribute.AttributeId.values()) {
            SharedTypes.getInstance().addAttribute(new ClickFicAttribute(attribute));
        }

        // TODO - load sound and music keys in SharedTypes
    }

}
