/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.config;

public enum ClickFicPropertyName {

    CLICKFIC_CONSOLE_FONT,
    CONSOLE_FONT_COLOR_ACTION,
    CONSOLE_FONT_COLOR_DIALOG,
    CONSOLE_FONT_COLOR_SYSTEM,
    CONSOLE_FONT_COLOR_READING,
    OPTION_BUTTON_FONT_SIZE,
    OPTION_BUTTON_FONT_COLOR,

    CURRENT_STORY

}
