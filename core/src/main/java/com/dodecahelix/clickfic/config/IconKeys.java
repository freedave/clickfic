/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.config;

import java.util.HashMap;
import java.util.Map;

import com.dodecahelix.ifengine.data.CommandType;
import com.dodecahelix.ifengine.options.ListGroup;
import com.dodecahelix.libgdx.template.view.resource.IconKey;

public class IconKeys {

    public static final IconKey BULLET_BLACK = new IconKey(10, "bullet_black");
    public static final IconKey BULLET_BLUE = new IconKey(11, "bullet_blue");
    public static final IconKey BULLET_GREEN = new IconKey(12, "bullet_green");
    public static final IconKey BULLET_ORANGE = new IconKey(13, "bullet_orange");
    public static final IconKey BULLET_PURPLE = new IconKey(14, "bullet_purple");
    public static final IconKey BULLET_RED = new IconKey(15, "bullet_red");
    public static final IconKey BULLET_WHITE = new IconKey(16, "bullet_white");
    public static final IconKey BULLET_YELLOW = new IconKey(17, "bullet_yellow");
    public static final IconKey FOLDER = new IconKey(18, "folder");
    public static final IconKey COG = new IconKey(19, "cog");
    public static final IconKey COMMENT = new IconKey(20, "comment");
    public static final IconKey HAND = new IconKey(21, "hand");

    public static Map<String, IconKey> lookupMap = new HashMap<String, IconKey>();

    static {
        lookupMap.put(CommandType.ACTION.name().toLowerCase(), HAND);
        lookupMap.put(CommandType.DIALOG.name().toLowerCase(), COMMENT);
        lookupMap.put(CommandType.LOOK.name().toLowerCase(), BULLET_PURPLE);
        lookupMap.put(CommandType.MOVE.name().toLowerCase(), BULLET_GREEN);
        lookupMap.put(CommandType.SYSTEM.name().toLowerCase(), BULLET_WHITE);
        lookupMap.put(ListGroup.INVENTORY.name().toLowerCase(), BULLET_BLACK);
        lookupMap.put(ListGroup.ITEM.name().toLowerCase(), BULLET_BLACK);
        lookupMap.put(ListGroup.MASS.name().toLowerCase(), BULLET_BLACK);
        lookupMap.put(ListGroup.MOVE.name().toLowerCase(), BULLET_BLACK);
        lookupMap.put(ListGroup.PERSON.name().toLowerCase(), BULLET_BLACK);
        lookupMap.put(ListGroup.READER.name().toLowerCase(), BULLET_BLACK);
        lookupMap.put(ListGroup.ROOM.name().toLowerCase(), BULLET_BLACK);
        lookupMap.put(ListGroup.SYSTEM.name().toLowerCase(), COG);
    }

    public static IconKey lookup(String lookupName) {
        return lookupMap.get(lookupName.toLowerCase());
    }

}
