/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.config;

public enum FloatConstants {

    FLOAT_PROP_A(0.1f);

    private float value;

    FloatConstants(float value) {
        this.value = value;
    }

    public float getValue() {
        return value;
    }

}
