/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.config;

/**
 *   Integer constants
 *
 */
public enum IntConstants {

    GRID_SIZE_TO_SCREEN_SIZE(3),
    BACKGROUND_GRID_ENTITY_CAP(50),
    BOOK_ENTITY_CAP(50),
    LIBRARY_SCREEN_ID(101);

    private int value;

    IntConstants(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
