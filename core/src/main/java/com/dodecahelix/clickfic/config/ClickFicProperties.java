/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.config;

import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.PropertyName;
import com.dodecahelix.libgdx.template.ui.RgbColor;
import com.dodecahelix.libgdx.template.view.resource.FontStyle;

/**
 *   Use to customize standard properties, or to add properties that can be configured through Options
 *
 * @author dpeters
 *
 */
public class ClickFicProperties {

    /**
     *   Overwrite default properties from gdx-template (PropertyName class) and set default values for optional properties.
     *
     * @param properties
     */
    public static void overwriteDefaults(Properties properties) {

        properties.addStringProperty(ClickFicPropertyName.CURRENT_STORY.name(), StringConstants.DEFAULT_STORY.getValue());

        // overloaded properties
        properties.addStringProperty(PropertyName.WELCOME_MESSAGE.name(), "ClickFic IF Platform Version 1.0");
        properties.addStringProperty(PropertyName.SPLASH_SCREEN_TEXTURE.name(), "textures/dodecahelix.png");
        properties.addScalableFloatProperty(PropertyName.UI_BIG_BUTTON_HEIGHT.name(), 60.0f);
        properties.addStringProperty(PropertyName.UI_DEFAULT_FONT.name(), FontStyle.ROBOTO.getDisplayName());
        properties.addScalableFloatProperty(PropertyName.UI_STANDARD_PADDING.name(), 16.0f);
        properties.addStringProperty(PropertyName.UI_BUTTON_FONT_COLOR.name(), RgbColor.BLACK.name());

        properties.addStringProperty(ClickFicPropertyName.CLICKFIC_CONSOLE_FONT.name(), FontStyle.ARIMO.getDisplayName());

        // Overridden for light background
        properties.addStringProperty(ClickFicPropertyName.CONSOLE_FONT_COLOR_ACTION.name(), RgbColor.GREY_SLATEGREY.name());
        properties.addStringProperty(ClickFicPropertyName.CONSOLE_FONT_COLOR_DIALOG.name(), RgbColor.BLUE_TEAL.name());
        properties.addStringProperty(ClickFicPropertyName.CONSOLE_FONT_COLOR_SYSTEM.name(), RgbColor.RED_CRIMSON.name());
        properties.addStringProperty(ClickFicPropertyName.CONSOLE_FONT_COLOR_READING.name(), RgbColor.BLACK.name());

        // For the option tree menu
        properties.addStringProperty(ClickFicPropertyName.OPTION_BUTTON_FONT_COLOR.name(), RgbColor.BLACK.name());
        properties.addScalableIntegerProperty(ClickFicPropertyName.OPTION_BUTTON_FONT_SIZE.name(), 18);

        // Overridden for light background
        properties.addTextureProperty(PropertyName.DEFAULT_BUTTON_UP_TEXTURE.name(), ClickFicTextureKey.BB_BUTTON);
        properties.addTextureProperty(PropertyName.DEFAULT_BUTTON_DOWN_TEXTURE.name(), ClickFicTextureKey.BB_DOWN_BUTTON);
        properties.addTextureProperty(PropertyName.DEFAULT_BUTTON_DISABLED_TEXTURE.name(), ClickFicTextureKey.BB_DISABLED_BUTTON);

        properties.addTextureProperty(PropertyName.DEFAULT_SCROLLBAR_FRAME_TEXTURE.name(), ClickFicTextureKey.BB_SCROLLBAR_FRAME);
        properties.addTextureProperty(PropertyName.DEFAULT_SCROLLBAR_BG_TEXTURE.name(), ClickFicTextureKey.BB_SCROLLBAR_SCROLL_BG);

        // Overridden for light background
        properties.addTextureProperty(PropertyName.DIALOG_FRAME_TEXTURE.name(), ClickFicTextureKey.BB_DIALOG_FRAME);

        // override the BG texture
        properties.addTextureProperty(PropertyName.UI_BACKGROUND_TILE.name(), ClickFicTextureKey.TILE_BACKGROUND);

        properties.addBooleanProperty(PropertyName.DEBUG_MODE.name(), false);
    }

}
