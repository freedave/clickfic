/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.config;

import com.dodecahelix.libgdx.template.view.resource.TextureKey;

public class ClickFicTextureKey extends TextureKey {

    // Main menu background
    public static final ClickFicTextureKey TILE_BACKGROUND = new ClickFicTextureKey(50, "pattern-gplay", 0);

    public static final ClickFicTextureKey BB_BUTTON = new ClickFicTextureKey(51, "vista-button-128-grey", 8);
    public static final ClickFicTextureKey BB_DOWN_BUTTON = new ClickFicTextureKey(52, "vista-button-128-grey-negative", 8);
    public static final ClickFicTextureKey BB_DISABLED_BUTTON = new ClickFicTextureKey(53, "vista-button-128-grey-sepia", 8);
    public static final ClickFicTextureKey BB_SCROLLBAR_FRAME = new ClickFicTextureKey(54, "squareish", 5);
    public static final ClickFicTextureKey BB_SCROLLBAR_SCROLL_BG = new ClickFicTextureKey(55, "scrollbar-vertical", 10);
    public static final ClickFicTextureKey BB_DIALOG_FRAME = new ClickFicTextureKey(57, "squareish", 5);

    public ClickFicTextureKey(int lookupId, String resourceId, int cornerWidth) {
        super(lookupId, resourceId, cornerWidth);
    }

}
