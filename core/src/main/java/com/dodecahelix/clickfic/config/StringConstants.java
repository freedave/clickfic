/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.config;

public enum StringConstants {

    SPLACH_SCREEN_TEXTURE("textures/splash-world.png"),
    LOGGING_TAG_CLICKFIC("clickfic"),
    LIBRARY_CONTROLLER_KEY("controller.storydata"),
    DEFAULT_STORY("pyramidman-anonymous-0.1.0");

    private String value;

    StringConstants(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
