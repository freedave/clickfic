/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.model.book;

import java.util.List;

import com.badlogic.gdx.utils.Array;
import com.dodecahelix.clickfic.config.IntConstants;
import com.dodecahelix.clickfic.model.ClickFicAttribute;
import com.dodecahelix.clickfic.model.options.Option;
import com.dodecahelix.clickfic.model.options.OptionTreeResponseBuilder;
import com.dodecahelix.clickfic.model.options.OptionsTreeEntity;
import com.dodecahelix.clickfic.system.SystemCommand;
import com.dodecahelix.ifengine.options.item.GroupListItem;
import com.dodecahelix.ifengine.response.StyledResponseLine;
import com.dodecahelix.ifengine.choice.ChoiceResult;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.control.events.StandardViewEventType;
import com.dodecahelix.libgdx.template.control.events.ViewEvent;
import com.dodecahelix.libgdx.template.model.Entity;
import com.dodecahelix.libgdx.template.model.SpaceId;
import com.dodecahelix.libgdx.template.model.SpaceType;
import com.dodecahelix.libgdx.template.model.interfaces.ConsoleEntity;
import com.dodecahelix.libgdx.template.model.interfaces.UiEntity;
import com.dodecahelix.libgdx.template.model.interfaces.UserInterfaceSpace;
import com.dodecahelix.libgdx.template.types.Attribute.StandardAttribute;
import com.dodecahelix.libgdx.template.types.EntityType.CommonEntity;

/**
 *
 * This encapsulates the Story view-model and interface with the IF Engine
 *
 */
public class BookSpace extends UserInterfaceSpace {

    /**
     * Creates the two view-model entities - the console and the optionTree
     */
    private BookSpaceEntityFactory entityFactory;

    /**
     * Entity ID of the console view-model
     */
    private long consoleId;

    /**
     * Entity ID of the option tree view-model
     */
    private long optionTreeId;

    /**
     * Processes IF engine commands coming from the option tree
     */
    private IfCommandHandler commandHandler;

    /**
     * Handles the sending of the response back to the view
     */
    private ConsoleResponseHandler consoleResponseHandler;

    /**
     * Builds the option tree for the response
     */
    private OptionTreeResponseBuilder optionTreeBuilder;

    private Properties applicationProperties;

    public BookSpace(BookSpaceEntityFactory entityFactory,
                     IfCommandHandler commandHandler,
                     OptionTreeResponseBuilder optionTreeBuilder,
                     Properties properties) {

        super(new SpaceId(SpaceType.USER_INTERFACE, 2), IntConstants.BOOK_ENTITY_CAP.getValue());

        this.applicationProperties = properties;
        this.entityFactory = entityFactory;

        this.commandHandler = commandHandler;
        this.optionTreeBuilder = optionTreeBuilder;
    }

    /**
     * Reset the space.
     *
     * <p>This is called:
     * <p>- when the model is initialized (when ClickFic application starts up) 
     * <p>- when the screen is (re)shown - you select Start from main menu
     */
    @Override
    public void reset() {
        this.clear();

        // build the model entities
        buildConsole();
        buildOptionsList();

        // load the color and font preferences
        consoleResponseHandler = new ConsoleResponseHandler(applicationProperties);

        // initialize the environment by loading up the model (needed by thes main menu and library)
        // don't start the story yet, though..
        commandHandler.getSystemCommandHandler().handleSystemCommand(SystemCommand.RESTART);
    }

    private long buildConsole() {
        Entity console = entityFactory.createNewEntity(CommonEntity.CONSOLE.getId(), this);
        consoleId = console.getId();

        console.getAttributes().setFloatValue(StandardAttribute.POSITION_X.getId(), 0.02f);
        console.getAttributes().setFloatValue(StandardAttribute.POSITION_Y.getId(), 0.40f);
        console.getAttributes().setFloatValue(StandardAttribute.HEIGHT.getId(), 0.58f);
        console.getAttributes().setBooleanValue(StandardAttribute.FILL_X.getId(), true);
        console.getAttributes().setBooleanValue(StandardAttribute.VISIBLE.getId(), true);

        return consoleId;
    }

    private long buildOptionsList() {
        OptionsTreeEntity options = (OptionsTreeEntity) entityFactory.createNewEntity(CommonEntity.OPTIONS.getId(), this);
        optionTreeId = options.getId();

        options.getAttributes().setFloatValue(StandardAttribute.POSITION_X.getId(), 0.02f);
        options.getAttributes().setFloatValue(StandardAttribute.POSITION_Y.getId(), 0.02f);
        options.getAttributes().setFloatValue(StandardAttribute.HEIGHT.getId(), 0.38f);
        options.getAttributes().setBooleanValue(StandardAttribute.FILL_X.getId(), true);
        options.getAttributes().setBooleanValue(StandardAttribute.VISIBLE.getId(), true);

        return optionTreeId;
    }

    @Override
    public void bump(float delta) {
        for (Entity entity : this.getAllEntities()) {
            if (entity.getId() == consoleId) {
            }
            if (entity.getId() == optionTreeId) {
            }
        }
    }

    @Override
    protected UiEntity buildNewEntity() {
        UiEntity entity = new UiEntity(this.getId());
        return entity;
    }

    @Override
    public void handleEvent(ViewEvent event) {

        if (event.getEventTypeId() == StandardViewEventType.SCREEN_SHOW.getEventTypeId()) {
            reset();

            // yes, we do this twice
            ChoiceResult startResult = commandHandler.getSystemCommandHandler().handleSystemCommand(SystemCommand.RESTART);
            endTurn(startResult);
        }

        if (event.getEventTypeId() == StandardViewEventType.BUTTON_CLICK.getEventTypeId()) {
            String commandId = event.getStringValue(ClickFicAttribute.AttributeId.COMMAND_ID.getAttributeCode());
            if (commandId != null) {
                if (SystemCommand.EXIT.getCommandId().equalsIgnoreCase(commandId)) {
                    // an exit has occurred.  Perform any cleanup (the screen will have turned back to the main menu)
                } else {
                    ChoiceResult choiceResult = commandHandler.processCommand(commandId);
                    endTurn(choiceResult);
                }
            }
        }
    }

    /**
     * After the ifEngine has processed the turn, this will update the view
     * models (the console and option tree)
     *
     * @param choiceResult
     */
    private void endTurn(ChoiceResult choiceResult) {
        List<StyledResponseLine> consoleLines = choiceResult.getResponseLines();
        sendConsoleResponse(choiceResult.getCommandDisplay(), consoleLines);

        Array<Option> options = null;
        GroupListItem optionTree = choiceResult.getOptionTree();
        if (optionTree != null) {
            options = optionTreeBuilder.resetTree(optionTree);
        }
        refreshOptionTree(options);
    }

    /**
     * Send a response to the console. Include a display of the command that has
     * just been executed
     *
     * @param commandDisplay
     *            - command that has just been executed
     * @param consoleLines
     *            - response lines from IF Engine after command/turn has been
     *            processed
     */
    private void sendConsoleResponse(String commandDisplay, List<StyledResponseLine> consoleLines) {

        for (Entity entity : this.getAllEntities()) {
            if (entity.getId() == consoleId) {
                ConsoleEntity console = (ConsoleEntity) entity;
                consoleResponseHandler.sendConsoleResponse(commandDisplay, consoleLines, console);
            }
        }

    }

    /**
     * Add a new set of values to the option tree display
     *
     * @param optionTree
     */
    private void refreshOptionTree(Array<Option> optionTree) {

        for (Entity entity : this.getAllEntities()) {
            if (entity.getId() == optionTreeId) {
                OptionsTreeEntity optionTreeEntity = (OptionsTreeEntity) entity;
                if (optionTree != null) {
                    optionTreeEntity.setOptions(optionTree);
                }

                // indicate out of synch, so the UI will refresh
                optionTreeEntity.setSynched(false);
            }
        }
    }

}
