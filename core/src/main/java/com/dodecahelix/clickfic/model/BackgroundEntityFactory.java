/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.model;

import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.model.AbstractEntityFactory;
import com.dodecahelix.libgdx.template.model.EntityState;
import com.dodecahelix.libgdx.template.model.Space;
import com.dodecahelix.libgdx.template.model.twodim.GridEntity;
import com.dodecahelix.libgdx.template.types.EntityType.CommonEntity;

public class BackgroundEntityFactory extends AbstractEntityFactory<GridEntity> {

    public BackgroundEntityFactory(Properties properties) {
        super(properties);
    }

    @Override
    protected GridEntity obtainEntity(int entityTypeId, Space<GridEntity> space) {
        GridEntity entity = space.getEntityPool().obtain();

        if (entityTypeId == CommonEntity.BACKGROUND_TILES.getId()) {
            entity.setup(CommonEntity.BACKGROUND_TILES.getId(), EntityState.BACKGROUND);
        }

        return entity;
    }

}
