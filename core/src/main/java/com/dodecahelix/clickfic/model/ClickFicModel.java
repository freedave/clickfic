/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.model;

import com.badlogic.gdx.utils.IntMap;
import com.dodecahelix.clickfic.model.book.BookSpace;
import com.dodecahelix.libgdx.template.model.Entity;
import com.dodecahelix.libgdx.template.model.Model;
import com.dodecahelix.libgdx.template.model.Space;
import com.dodecahelix.libgdx.template.model.SpaceId;
import com.dodecahelix.libgdx.template.model.SpaceType;

public class ClickFicModel extends Model {

    BookSpace uiSpace;
    BackgroundGridSpace backgroundGrid;

    public static final int BACKGROUND_ID = 1;
    public static final int BOOK_ID = 2;

    public static final IntMap<SpaceId> spaceMap;

    static {
        spaceMap = new IntMap<SpaceId>();
        spaceMap.put(BACKGROUND_ID, new SpaceId(SpaceType.GRID, BACKGROUND_ID));
        spaceMap.put(BOOK_ID, new SpaceId(SpaceType.USER_INTERFACE, BOOK_ID));
    }

    private SpaceId[] spaces;

    public ClickFicModel(BookSpace uiSpace, BackgroundGridSpace backgroundGrid) {
        super();

        this.uiSpace = uiSpace;
        this.backgroundGrid = backgroundGrid;

        // ugly code to get around a class cast bug
        spaces = new SpaceId[spaceMap.size];
        int i = 0;
        for (SpaceId id : spaceMap.values()) {
            spaces[i] = id;
            i++;
        }
    }

    @Override
    public SpaceId[] spaces() {
        return spaces;
    }

    @Override
    public Space<? extends Entity> createSpace(SpaceId id) {
        switch (id.getPrimitiveId()) {
            case BACKGROUND_ID:
                return backgroundGrid;
            case BOOK_ID:
                return uiSpace;
        }
        return null;
    }

}
