/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.model.book;

import java.util.List;

import com.dodecahelix.clickfic.config.ClickFicPropertyName;
import com.dodecahelix.ifengine.response.ResponseType;
import com.dodecahelix.ifengine.response.StyledResponseLine;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.model.interfaces.ConsoleEntity;
import com.dodecahelix.libgdx.template.text.TextLine;
import com.dodecahelix.libgdx.template.ui.RgbColor;

public class ConsoleResponseHandler {

    private Properties properties;

    private RgbColor systemFontColor;
    private RgbColor readingFontColor;
    private RgbColor actionFontColor;
    private RgbColor dialogFontColor;

    public ConsoleResponseHandler(Properties properties) {
        this.properties = properties;

        resetFonts();
    }

    public void resetFonts() {

        // these colors are configurable through options
        String systemFontColorString = properties.getStringProperty(ClickFicPropertyName.CONSOLE_FONT_COLOR_SYSTEM.name());
        systemFontColor = RgbColor.valueOf(systemFontColorString);

        String dialogFontColorString = properties.getStringProperty(ClickFicPropertyName.CONSOLE_FONT_COLOR_DIALOG.name());
        dialogFontColor = RgbColor.valueOf(dialogFontColorString);

        String actionFontColorString = properties.getStringProperty(ClickFicPropertyName.CONSOLE_FONT_COLOR_ACTION.name());
        actionFontColor = RgbColor.valueOf(actionFontColorString);

        String readingFontColorString = properties.getStringProperty(ClickFicPropertyName.CONSOLE_FONT_COLOR_READING.name());
        readingFontColor = RgbColor.valueOf(readingFontColorString);
    }

    public void sendConsoleResponse(String commandDisplay,
                                    List<StyledResponseLine> consoleLines,
                                    ConsoleEntity console) {

        // for silent commands, don't send out a line of text
        if (commandDisplay != null) {
            console.addNewLine(new TextLine("> " + commandDisplay, null, null, null, actionFontColor));
        }

        for (StyledResponseLine line : consoleLines) {
            RgbColor lineColor;
            switch (line.getType()) {
                case ACTION:
                    lineColor = actionFontColor;
                    break;
                case DIALOG:
                    lineColor = dialogFontColor;
                    break;
                case SYSTEM:
                    lineColor = systemFontColor;
                    break;
                case PAUSE:
                    lineColor = systemFontColor;
                    break;
                default:
                    lineColor = readingFontColor;
            }

            TextLine newLine = new TextLine(line.getText(), null, null, null, lineColor);
            if (ResponseType.PAUSE == line.getType()) {
                newLine.setPause(true);
            }
            console.addNewLine(newLine);
        }

        // the model has changed, and so the model is out of synch with the view
        console.setSynched(false);
    }

}
