/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.model.options;

import com.badlogic.gdx.utils.Array;

public class Option {

    // whether this is an action or not
    private String command;
    private String optionType;
    private String display;

    /**
     *   Whether this is a group holder (i.e; folder) option
     */
    private boolean groupOption = false;
    private Array<Option> childOptions;

    public Option(String display) {
        this.display = display;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public String getDisplay() {
        return display;
    }

    public void setDisplay(String display) {
        this.display = display;
    }

    public Array<Option> getChildOptions() {
        return childOptions;
    }

    public void setChildOptions(Array<Option> childOptions) {
        this.childOptions = childOptions;
    }

    public boolean isGroupOption() {
        return groupOption;
    }

    public void setGroupOption(boolean groupOption) {
        this.groupOption = groupOption;
    }

    public String getOptionType() {
        return optionType;
    }

    public void setOptionType(String commandType) {
        this.optionType = commandType;
    }

}
