/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.model.book;

import com.dodecahelix.clickfic.model.options.OptionsTreeEntity;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.model.AbstractEntityFactory;
import com.dodecahelix.libgdx.template.model.EntityState;
import com.dodecahelix.libgdx.template.model.Space;
import com.dodecahelix.libgdx.template.model.interfaces.ConsoleEntity;
import com.dodecahelix.libgdx.template.model.interfaces.UiEntity;
import com.dodecahelix.libgdx.template.types.EntityType.CommonEntity;

/**
 *   Creates a new entity of the given type in the given space
 *    (may not be initialized)
 *
 * @author dpeters
 *
 */
public class BookSpaceEntityFactory extends AbstractEntityFactory<UiEntity> {

    public BookSpaceEntityFactory(Properties properties) {
        super(properties);
    }

    @Override
    protected UiEntity obtainEntity(int entityTypeId, Space<UiEntity> space) {

        UiEntity entity = null;

        if (entityTypeId == CommonEntity.CONSOLE.getId()) {
            // don't really need a pool here
            entity = new ConsoleEntity(space.getId());
            entity.setup(CommonEntity.CONSOLE.getId(), EntityState.ACTIVE);
        }

        if (entityTypeId == CommonEntity.OPTIONS.getId()) {
            entity = new OptionsTreeEntity(space.getId());
            entity.setup(CommonEntity.OPTIONS.getId(), EntityState.ACTIVE);
        }

        return entity;
    }

}
