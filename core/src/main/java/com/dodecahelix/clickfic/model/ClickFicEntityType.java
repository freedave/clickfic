/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.model;

import com.dodecahelix.libgdx.template.types.EntityType;

public class ClickFicEntityType extends EntityType {

    public enum ClickFicEntity {

        // This is a sample entity for reference
        APPLICATION_ENTITY(100);

        private int typeCode;

        ClickFicEntity(int typeCode) {
            this.typeCode = typeCode;
        }

        public int getTypeCode() {
            return typeCode;
        }
    }

    public ClickFicEntityType(String name, int referenceCode) {
        super(name, referenceCode);
    }

    public ClickFicEntityType(ClickFicEntity entity) {
        super(entity.name(), entity.getTypeCode());
    }

}
