/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.model.book;

import com.badlogic.gdx.Gdx;
import com.dodecahelix.clickfic.config.StringConstants;
import com.dodecahelix.clickfic.model.options.OptionTreeResponseBuilder;
import com.dodecahelix.clickfic.system.SystemCommand;
import com.dodecahelix.clickfic.system.SystemCommandHandler;
import com.dodecahelix.ifengine.IfEngine;
import com.dodecahelix.ifengine.options.item.CommandListItem;
import com.dodecahelix.ifengine.options.item.DialogListItem;
import com.dodecahelix.ifengine.choice.ChoiceResult;
import com.dodecahelix.libgdx.template.config.SharedConstants;

/**
 *  Processes IF (interactive fiction) commands using the IfEngine library interfaces
 *
 * @author dpeters
 *
 */
public class IfCommandHandler {

    private OptionTreeResponseBuilder optionTreeBuilder;
    private SystemCommandHandler systemCommandHandler;
    private IfEngine ifEngine;

    public IfCommandHandler(IfEngine ifEngine,
                            OptionTreeResponseBuilder optionTreeBuilder,
                            SystemCommandHandler systemCommandHandler) {

        this.ifEngine = ifEngine;
        this.optionTreeBuilder = optionTreeBuilder;
        this.systemCommandHandler = systemCommandHandler;
    }

    public void reset() {
    }

    public ChoiceResult processCommand(String commandId) {
        Gdx.app.log(StringConstants.LOGGING_TAG_CLICKFIC.getValue(), "handling command " + commandId);

        ChoiceResult choiceResult = null;

        // TODO - is there a better way than passing command ID?
        if (commandId.startsWith("system.")) {
            // handle system command
            SystemCommand command = SystemCommand.getByCommandId(commandId);
            String commandDisplay = command.getLabel();
            Gdx.app.log(SharedConstants.LOGGING_TAG_APPLICATION, "Handling system command : " + commandDisplay);

            // this may or may not produce a result (new option tree and console output) - may return null
            choiceResult = systemCommandHandler.handleSystemCommand(command);
            if (choiceResult != null) {
                choiceResult.setCommandDisplay(commandDisplay);
            }
        } else {
            if (commandId.startsWith(DialogListItem.DIALOG_COMMAND_ID)) {
                DialogListItem dialogItem = optionTreeBuilder.getDialog(commandId);
                String dialogDisplay = dialogItem.getLabel();
                Gdx.app.log(SharedConstants.LOGGING_TAG_GAME, "Handling dialog : " + dialogDisplay);

                choiceResult = ifEngine.getChoiceProcessor().processDialog(dialogItem.getDialog(), dialogItem.getPersonId());
                choiceResult.setCommandDisplay(dialogDisplay);
            } else {
                // standard command
                CommandListItem commandItem = optionTreeBuilder.getCommand(commandId);

                String commandDisplay = commandItem.getLabel();
                Gdx.app.log(SharedConstants.LOGGING_TAG_GAME, "Handling command : " + commandDisplay);
                choiceResult = ifEngine.getChoiceProcessor().processCommand(commandItem.getCommand(), commandItem.getOwnerId(), commandItem.getTargetId());
                choiceResult.setCommandDisplay(commandDisplay);
            }
        }

        return choiceResult;
    }

    public SystemCommandHandler getSystemCommandHandler() {
        return systemCommandHandler;
    }

}
