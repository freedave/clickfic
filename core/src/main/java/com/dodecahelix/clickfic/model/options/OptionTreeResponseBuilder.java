/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.model.options;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.badlogic.gdx.utils.Array;
import com.dodecahelix.clickfic.system.SystemCommand;
import com.dodecahelix.ifengine.data.CommandType;
import com.dodecahelix.ifengine.options.ListGroup;
import com.dodecahelix.ifengine.options.item.CommandListItem;
import com.dodecahelix.ifengine.options.item.DialogListItem;
import com.dodecahelix.ifengine.options.item.GroupListItem;
import com.dodecahelix.ifengine.options.item.ListItem;
import com.dodecahelix.ifengine.options.item.SystemListItem;
import com.dodecahelix.ifengine.util.StringUtils;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.PropertyName;

public class OptionTreeResponseBuilder {

    /**
     *   Maps the command list item ID to the Command, so that the view DTO does not need to reference the command itself
     */
    private Map<String, CommandListItem> commandMap;

    /**
     *   Maps the dialog list item to the Dialog
     */
    private Map<String, DialogListItem> dialogMap;

    private Properties properties;

    public OptionTreeResponseBuilder(Properties properties) {
        commandMap = new HashMap<String, CommandListItem>();
        dialogMap = new HashMap<String, DialogListItem>();

        this.properties = properties;
    }

    public Array<Option> resetTree(GroupListItem rootItem) {
        commandMap.clear();
        dialogMap.clear();

        Array<Option> options = buildOptionTree(rootItem);

        // add system options
        GroupListItem systemTree = new GroupListItem(ListGroup.SYSTEM, "System Actions");
        List<ListItem> systemActions = buildSystemActions();
        systemTree.setChildListItems(systemActions);

        Option option = new Option(systemTree.getLabel());
        option.setChildOptions(buildOptionTree(systemTree));
        options.add(option);

        return options;
    }

    private List<ListItem> buildSystemActions() {
        List<ListItem> systemActions = new ArrayList<ListItem>();

        boolean isDebugMode = properties.getBooleanProperty(PropertyName.DEBUG_MODE.name());

        for (SystemCommand command : SystemCommand.values()) {
            if (!command.isDebug() || isDebugMode) {
                ListItem systemListItem = new SystemListItem(command.getCommandId(), command.getLabel());
                systemActions.add(systemListItem);
            }
        }

        return systemActions;
    }

    private Array<Option> buildOptionTree(GroupListItem groupItem) {

        Array<Option> options = new Array<Option>(groupItem.getChildListItems().size());
        for (ListItem item : groupItem.getChildListItems()) {
            String itemId = item.getItemId();

            // capitalize label?
            String capitalizedLabel = StringUtils.capitalize(item.getLabel());
            Option option = new Option(capitalizedLabel);
            option.setCommand(itemId);
            if (item instanceof GroupListItem) {
                GroupListItem childGroupItem = (GroupListItem) item;

                // don't add the group item if there are no children
                if (!childGroupItem.getChildListItems().isEmpty()) {
                    option.setChildOptions(buildOptionTree(childGroupItem));
                    option.setOptionType(childGroupItem.getGroupType().name());
                    option.setGroupOption(true);
                    options.add(option);
                }
            }
            if (item instanceof CommandListItem) {
                CommandListItem childCommandItem = (CommandListItem) item;
                String commandType = childCommandItem.getCommand().getCommandType().name();
                commandMap.put(itemId, childCommandItem);
                option.setOptionType(commandType);
                options.add(option);
            }
            if (item instanceof DialogListItem) {
                dialogMap.put(itemId, (DialogListItem) item);
                option.setOptionType(CommandType.DIALOG.name());
                options.add(option);
            }
            if (item instanceof SystemListItem) {
                option.setOptionType(CommandType.SYSTEM.name());
                options.add(option);
            }
        }
        return options;
    }

    public CommandListItem getCommand(String commandId) {
        return commandMap.get(commandId);
    }

    public DialogListItem getDialog(String dialogId) {
        return dialogMap.get(dialogId);
    }

}
