/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.model.options;

import com.badlogic.gdx.utils.Array;
import com.dodecahelix.libgdx.template.model.Entity;
import com.dodecahelix.libgdx.template.model.SpaceId;
import com.dodecahelix.libgdx.template.model.interfaces.UiEntity;

/**
 *   Holds the list of options available with each option potentially containing sub-options
 *
 */
public class OptionsTreeEntity extends UiEntity {

    private Array<Option> options;

    public OptionsTreeEntity(SpaceId space) {
        super(space);

        options = new Array<Option>();
    }

    @Override
    public void synchronizeStaticFields(Entity modelEntity) {
        if (!modelEntity.isSynched()) {
            // we only need to cast when its out of sync
            OptionsTreeEntity optionsTree = (OptionsTreeEntity) modelEntity;

            // this will make the view options the same object as entity options - scary but okay, since this is read only data
            this.options = optionsTree.getOptions();
            modelEntity.setSynched(true);

            // now the view is out of sync with its own model.  use this to tell it to refresh
            this.setSynched(false);
        }
    }

    public Array<Option> getOptions() {
        return options;
    }

    public void setOptions(Array<Option> options) {
        this.options = options;
    }

    public void addOption(Option option) {
        options.add(option);
    }

}
