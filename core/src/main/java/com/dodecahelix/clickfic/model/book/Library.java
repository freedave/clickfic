/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.model.book;

import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;

import com.dodecahelix.clickfic.PlatformDAO;
import com.dodecahelix.clickfic.PlatformException;
import com.dodecahelix.clickfic.config.ClickFicPropertyName;
import com.dodecahelix.ifengine.data.serializer.StoryBundle;
import com.dodecahelix.ifengine.data.story.LibraryCard;
import com.dodecahelix.ifengine.environment.EnvironmentProcessor;
import com.dodecahelix.ifengine.util.StoryFileNameUtil;
import com.dodecahelix.libgdx.template.config.SharedConstants;
import com.dodecahelix.libgdx.template.config.UserPreferences;

/**
 *   Provides access to the library
 *
 *   Maintains a reference to the current story being read
 *   Provides a list of other stories based on query criteria
 *   Allows the current story to be changed
 *
 * @author dpeters
 *
 */
public class Library {

    private PlatformDAO platform;
    private EnvironmentProcessor environmentProcessor;

    public Library(PlatformDAO platform, EnvironmentProcessor environmentProcessor) {

        this.platform = platform;
        this.environmentProcessor = environmentProcessor;
    }

    public LibraryCard getCurrentStory() {
        return environmentProcessor.getCurrentStoryInfo();
    }

    public List<LibraryCard> getAvailableStories() {
        try {
            return platform.retrieveAdditionalStories(StoryQuery.ALL);
        } catch (PlatformException e) {
            e.printStackTrace();
            return new ArrayList<LibraryCard>();
        }
    }

    public void checkoutNewTitle(LibraryCard selectedLibraryCard) {

        // get filename from title, author and version
        String storyFileName = StoryFileNameUtil.getStoryFileName(selectedLibraryCard);

        // get the data file using the title..
        try {
            StoryBundle story = platform.loadStoryFile(storyFileName);
            environmentProcessor.loadStory(story);

            // update preferences with new story
            UserPreferences preferences = platform.loadUserPreferences();
            preferences.getStringPreferences().put(ClickFicPropertyName.CURRENT_STORY.name(), storyFileName);
            platform.saveUserPreferences(preferences);
            Gdx.app.debug(SharedConstants.LOGGING_TAG_APPLICATION, "Updated current story preference to : " + storyFileName);

        } catch (PlatformException e) {
            Gdx.app.error(SharedConstants.LOGGING_TAG_APPLICATION, "Unable to load serialized story with filename: " + storyFileName);
            e.printStackTrace();
        }

    }

}
