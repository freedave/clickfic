/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.model;

import com.badlogic.gdx.Gdx;
import com.dodecahelix.clickfic.config.IntConstants;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.SharedConstants;
import com.dodecahelix.libgdx.template.control.events.StandardViewEventType;
import com.dodecahelix.libgdx.template.control.events.ViewEvent;
import com.dodecahelix.libgdx.template.math.twodim.Bounds;
import com.dodecahelix.libgdx.template.math.twodim.FixedPoint;
import com.dodecahelix.libgdx.template.model.SpaceId;
import com.dodecahelix.libgdx.template.model.SpaceType;
import com.dodecahelix.libgdx.template.model.twodim.Grid;
import com.dodecahelix.libgdx.template.model.twodim.GridEntity;
import com.dodecahelix.libgdx.template.types.EntityType.CommonEntity;

/**
 * @author dpeters
 *
 */
public class BackgroundGridSpace extends Grid {

    BackgroundEntityFactory entityFactory;

    Properties properties;

    private Bounds bounds;
    private Bounds viewportBounds;

    public BackgroundGridSpace(BackgroundEntityFactory entityFactory, Properties properties) {
        super(new SpaceId(SpaceType.GRID, 1), IntConstants.BACKGROUND_GRID_ENTITY_CAP.getValue());

        this.entityFactory = entityFactory;
        this.properties = properties;

        float screenWidth = Gdx.graphics.getWidth();
        float screenHeight = Gdx.graphics.getHeight();

        // This is an arbitray method -- Your grid can be any size that you like
        float width = screenWidth * IntConstants.GRID_SIZE_TO_SCREEN_SIZE.getValue();
        float height = screenHeight * IntConstants.GRID_SIZE_TO_SCREEN_SIZE.getValue();

        // Grid size may be set up through configuration
        bounds = new Bounds(width, height);

        // for this grid, viewport will be fixed to the center of the grid
        float bottomLeftX = width / 2 - screenWidth / 2;
        float bottomLeftY = height / 2 - screenHeight / 2;
        float topRightX = width / 2 + screenWidth / 2;
        float topRightY = height / 2 + screenHeight / 2;
        viewportBounds = new Bounds(new FixedPoint(bottomLeftX, bottomLeftY), new FixedPoint(topRightX, topRightY));
    }

    @Override
    public Bounds getGridBounds() {
        return bounds;
    }

    @Override
    public Bounds getViewportBounds() {
        return viewportBounds;
    }

    @Override
    public void reset() {
        this.clear();

        // The backgound tiles should be a "phantom" entity
        entityFactory.createNewEntity(CommonEntity.BACKGROUND_TILES.getId(), this);

        Gdx.app.debug(SharedConstants.LOGGING_TAG_LIFECYCLE, "Reset BackgroundGrid with Grid bounds : " + bounds + " and Viewport bounds : " + viewportBounds);
    }

    @Override
    /**
     *  Update the model for each frame (abstract this part away)
     */
    public void bump(float delta) {
        // happens every frame
    }

    @Override
    protected GridEntity buildNewEntity() {
        GridEntity entity = new GridEntity(this.getId());
        return entity;
    }

    @Override
    public void handleEvent(ViewEvent event) {
        if (event.getEventTypeId() == StandardViewEventType.SCREEN_SHOW.getEventTypeId()) {
            // when you open up the screen, reset the model and reshow the entities
            this.reset();
        }
    }

}
