/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.model;

import com.dodecahelix.libgdx.template.model.attributes.AttributeType;
import com.dodecahelix.libgdx.template.types.Attribute;

public class ClickFicAttribute extends Attribute {

    public enum AttributeId {

        COMMAND_ID(100, AttributeType.STRING);

        private int attributeCode;
        private AttributeType type;

        AttributeId(int attributeCode, AttributeType type) {
            this.attributeCode = attributeCode;
            this.type = type;
        }

        public int getAttributeCode() {
            return attributeCode;
        }

        public AttributeType getAttributeType() {
            return type;
        }
    }

    public ClickFicAttribute(String name, int code, AttributeType type) {
        super(name, code, type);
    }

    public ClickFicAttribute(AttributeId att) {
        super(att.name(), att.getAttributeCode(), att.getAttributeType());
    }

}
