/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic;

import java.util.List;

import com.dodecahelix.clickfic.model.book.StoryQuery;
import com.dodecahelix.ifengine.data.save.EnvironmentSave;
import com.dodecahelix.ifengine.data.serializer.StoryBundle;
import com.dodecahelix.ifengine.data.story.LibraryCard;
import com.dodecahelix.ifengine.choice.ChoiceRecord;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.UserPreferences;

/**
 *   Handles operations which depend on the client platform (i.e; Android, iOS or PC)
 *
 */
public interface PlatformDAO {

    public boolean isMobile();

    public StoryBundle loadStoryFile(String dataFileName) throws PlatformException;

    public EnvironmentSave restoreSavedProgress(String name) throws PlatformException;

    public void saveProgress(EnvironmentSave environmentSaveData) throws PlatformException;

    public UserPreferences loadUserPreferences() throws PlatformException;

    public void saveUserPreferences(UserPreferences preferences) throws PlatformException;

    public List<LibraryCard> retrieveAdditionalStories(StoryQuery query) throws PlatformException;

    public void saveChoiceRecord(List<ChoiceRecord> record) throws PlatformException;

    public List<ChoiceRecord> getChoiceRecord() throws PlatformException;

    /**
     *   Sets any properties that are specific the the platform (i.e; debug mode)
     *
     * @param properties
     * @throws PlatformException
     */
    public void initializeProperties(Properties properties) throws PlatformException;


}
