This software makes use of third party products and services using the following licenses (in addition to those listed in licenses-thirdparty.txt)

Google GSON:
URL : https://github.com/google/gson
License : Apache Version 2.0
License URL : https://github.com/google/gson/blob/master/LICENSE

WebLAF
URL : http://weblookandfeel.com/
License : Commercial, single application purchased by David Peters -  freedavep@gmail.com
License URL : http://weblookandfeel.com/alee-software-license-agreement/

MigLayout
Version : 3.7.3
URL : http://www.miglayout.com/
License : BSD
License URL : https://opensource.org/licenses/BSD-3-Clause

Google Guava
URL : https://github.com/google/guava
License : Apache 2
License URL : http://www.apache.org/licenses/LICENSE-2.0

Logback
URL : http://logback.qos.ch/
License : Eclipse Public License v1.0
License URL : http://logback.qos.ch/license.html




