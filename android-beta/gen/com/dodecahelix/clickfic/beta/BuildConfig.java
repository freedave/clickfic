/*******************************************************************************
 * Copyright (c) 2015 David Peters
 *
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.  
 * The details of these terms are listed in the LICENSE.txt 
 * which is located at the base of this project's source repository.
 *******************************************************************************/
/** Automatically generated file. DO NOT MODIFY */
package com.dodecahelix.clickfic.beta;

public final class BuildConfig {
    public final static boolean DEBUG = true;
}