/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.beta;

import android.content.Context;

import com.dodecahelix.clickfic.PlatformDAO;
import com.dodecahelix.clickfic.PlatformException;
import com.dodecahelix.clickfic.model.book.StoryQuery;
import com.dodecahelix.ifengine.choice.ChoiceRecord;
import com.dodecahelix.ifengine.data.save.EnvironmentSave;
import com.dodecahelix.ifengine.data.serializer.StoryBundle;
import com.dodecahelix.ifengine.data.story.LibraryCard;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.UserPreferences;

import java.util.List;

public class AndroidPlaformDAO implements PlatformDAO {

    private EnvironmentDAO environmentDAO;
    private SaveProgressDAO saveProgressDAO;
    private UserPreferencesDAO userPreferencesDAO;
    private LibraryDAO libraryDAO;

    public AndroidPlaformDAO(Context context) {

        environmentDAO = new EnvironmentDAO(context);
        saveProgressDAO = new SaveProgressDAO(context);
        userPreferencesDAO = new UserPreferencesDAO(context);
        libraryDAO = new AssetLibraryDAO(context);
    }

    @Override
    public boolean isMobile() {
        return true;
    }

    @Override
    public StoryBundle loadStoryFile(String dataFileName) throws PlatformException {
        return environmentDAO.loadStoryFile(dataFileName);
    }

    @Override
    public EnvironmentSave restoreSavedProgress(String name) throws PlatformException {

        return saveProgressDAO.restoreSavedProgress(name);
    }

    @Override
    public void saveProgress(EnvironmentSave environmentSaveData) throws PlatformException {
        saveProgressDAO.saveProgress(environmentSaveData);
    }

    @Override
    public UserPreferences loadUserPreferences() throws PlatformException {

        return userPreferencesDAO.loadUserPreferences();
    }

    @Override
    public void saveUserPreferences(UserPreferences preferences) throws PlatformException {

        userPreferencesDAO.saveUserPreferences(preferences);
    }

    @Override
    public List<LibraryCard> retrieveAdditionalStories(StoryQuery query) throws PlatformException {
        return libraryDAO.retrieveAdditionalStories(query);
    }

    @Override
    public void saveChoiceRecord(List<ChoiceRecord> record) throws PlatformException {
        throw new IllegalStateException("not allowed to save records on Android (DEBUG MODE property should be false)");
    }

    @Override
    public List<ChoiceRecord> getChoiceRecord() throws PlatformException {
        throw new IllegalStateException("not allowed to retrieve turn records on Android (DEBUG MODE property should be false)");
    }

    @Override
    public void initializeProperties(Properties properties) throws PlatformException {
    }

}
