/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.beta;

import android.content.Context;
import android.content.res.AssetManager;

import com.badlogic.gdx.Gdx;
import com.dodecahelix.clickfic.PlatformException;
import com.dodecahelix.clickfic.model.book.StoryQuery;
import com.dodecahelix.ifengine.data.story.LibraryCard;
import com.dodecahelix.ifengine.util.IoUtils;
import com.dodecahelix.libgdx.template.config.SharedConstants;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;

public class AssetLibraryDAO implements LibraryDAO {

    private Context context;
    private Gson gson;

    public AssetLibraryDAO(Context context) {
        this.context = context;
        gson = new Gson();
    }

    @Override
    public List<LibraryCard> retrieveAdditionalStories(StoryQuery query) throws PlatformException {

        Gdx.app.debug(SharedConstants.LOGGING_TAG_LIFECYCLE, "Loading library catalogue using query " + query.name());

        try {
            AssetManager am = context.getAssets();
            InputStream is = am.open("data/library.json");

            String jsonString = IoUtils.readFileFromInputStreamAsString(is);

            Type storyMetadataListType = new TypeToken<Collection<LibraryCard>>() {
            }.getType();
            List<LibraryCard> stories = gson.fromJson(jsonString, storyMetadataListType);

            Gdx.app.debug(SharedConstants.LOGGING_TAG_APPLICATION, "Loaded library catalogue : " + jsonString);

            return stories;
        } catch (IOException e) {
            throw new PlatformException(e);
        }


    }

}
