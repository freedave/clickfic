/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.beta;

import android.content.Context;
import com.dodecahelix.clickfic.PlatformException;
import com.dodecahelix.ifengine.data.save.EnvironmentSave;
import com.dodecahelix.ifengine.data.save.json.JsonEnvironmentSave;
import com.dodecahelix.ifengine.util.IoUtils;

import java.io.File;
import java.io.IOException;

public class SaveProgressDAO {

    private Context context;

    public SaveProgressDAO(Context context) {
        this.context = context;
    }

    public EnvironmentSave restoreSavedProgress(String storyId) throws PlatformException {

        try {
            File file = new File(context.getFilesDir(), storyId);
            String fileAsString = IoUtils.readFileAsString(file);

            return new JsonEnvironmentSave(storyId, fileAsString);
        } catch (IOException e) {
            throw new PlatformException("error restoring saved progress " + storyId, e);
        }
    }

    public void saveProgress(EnvironmentSave progress) throws PlatformException {

        try {
            File file = new File(context.getFilesDir(), progress.getName());

            String contentString = ((JsonEnvironmentSave) progress).getContent();
            IoUtils.writeFileFromString(file, contentString);
        } catch (IOException e) {
            throw new PlatformException("exception saving file to filesystem", e);
        }

    }


}
