/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import com.dodecahelix.ifengine.data.save.EnvironmentSave;
import com.dodecahelix.ifengine.data.save.json.JsonEnvironmentSave;
import com.dodecahelix.ifengine.util.IoUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *  Save the file to the desktop filesystem.
 */
public class JsonSavedProgressHandler implements SavedProgressHandler {

    static final Logger logger = LoggerFactory.getLogger(JsonSavedProgressHandler.class.getName());

    /**
     *  Retrieves the SaveGame from the filesystem
     */
    @Override
    public EnvironmentSave restoreSavedProgress(String fileName) throws PlatformException {
        try {
            File file = getSaveFile(fileName);
            String saveGame = IoUtils.readFileAsString(file);

            return new JsonEnvironmentSave(fileName, saveGame);
        } catch (IOException e) {
            throw new PlatformException(e);
        }
    }

    @Override
    public void saveProgress(EnvironmentSave environmentSave) throws PlatformException {
        PrintWriter out = null;
        try {
            File file = getSaveFile(environmentSave.getName());
            out = new PrintWriter(file);

            JsonEnvironmentSave jsonEnvironmentSave = (JsonEnvironmentSave) environmentSave;
            out.println(jsonEnvironmentSave.getContent());
        } catch (Exception e) {
            throw new PlatformException(e);
        } finally {
            if (out != null) {
                out.close();
            }
        }
    }

    private File getSaveFile(String fileName) throws IOException {
        // load prefs from user.dir, if it exists
        String userDir = System.getProperty("user.home");

        // does .ifebuilder directory exist?
        File clickficDir = new File(userDir + System.getProperty("file.separator") + Constants.CLICKFIC_DIR);

        File saveFile = new File(clickficDir.getAbsolutePath() + System.getProperty("file.separator") + fileName);
        if (!saveFile.exists()) {
            logger.debug("saved progress file not found.  creating.");
            saveFile.createNewFile();
        }

        logger.debug("saved progress file found at location " + saveFile.getAbsolutePath());
        return saveFile;
    }

}
