/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic;

import java.io.IOException;
import java.util.List;

import com.dodecahelix.ifengine.data.serializer.CsvStoryBundle;
import com.dodecahelix.ifengine.data.serializer.JsonStoryBundle;
import com.dodecahelix.ifengine.data.serializer.MediaFormat;
import com.dodecahelix.ifengine.data.serializer.StoryBundle;
import com.dodecahelix.ifengine.util.IoUtils;

public class EnvironmentHandler {

    private static final MediaFormat DATA_FORMAT = MediaFormat.JSON;

    public StoryBundle loadStoryFile(String storyName) throws PlatformException {

        StoryBundle story = null;
        switch (DATA_FORMAT) {
            case JSON:
                story = loadStoryJsonFile(storyName);
                break;
            case CSV:
                story = loadStoryCsvFile(storyName);
                break;
            default:
                throw new IllegalStateException("Data format not supported for story file : " + DATA_FORMAT);
        }

        return story;
    }

    private StoryBundle loadStoryJsonFile(String storyName) throws PlatformException {

        String fullStoryName = "data/" + storyName + ".json";

        try {
            String jsonString = IoUtils.readFileAsString(fullStoryName);

            System.out.println("json is " + jsonString);
            return new JsonStoryBundle(jsonString);
        } catch (IOException e) {
            throw new PlatformException(e);
        }
    }

    @Deprecated
    private StoryBundle loadStoryCsvFile(String storyName) throws PlatformException {

        String fullStoryName = storyName + ".csv";
        try {
            List<String> stringList = IoUtils.readFileAsStringList(fullStoryName);
            return new CsvStoryBundle(stringList);
        } catch (IOException e) {
            throw new PlatformException(e);
        }
    }

}
