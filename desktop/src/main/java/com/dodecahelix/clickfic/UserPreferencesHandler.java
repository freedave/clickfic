/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Map.Entry;
import java.util.Properties;

import com.dodecahelix.libgdx.template.config.UserPreferences;

public class UserPreferencesHandler {

    private static final String USER_PREFS_PATH = System.getProperty("user.home") + System.getProperty("file.separator") + ".clickfic"
    + System.getProperty("file.separator") + "desktop.properties";

    public UserPreferencesHandler() {

        // create the user dir folder if it doesnt yet exist
        UserDirTool.checkForPrivateUserDir();

        // create properties file if it doesnt exist
        File file = new File(USER_PREFS_PATH);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public UserPreferences loadUserPreferences() {

        UserPreferences prefs = new UserPreferences();
        try {
            FileReader reader = new FileReader(USER_PREFS_PATH);
            if (reader != null) {
                Properties props = new Properties();
                props.load(reader);

                reader.close();

                for (Entry<Object, Object> property : props.entrySet()) {
                    String propertyName = property.getKey().toString();
                    String propertyType = propertyName.substring(0, 3);
                    propertyName = propertyName.substring(4);

                    if ("bln".equalsIgnoreCase(propertyType)) {
                        prefs.getBooleanPreferences().put(propertyName, getBoolean(property));
                    }
                    if ("str".equalsIgnoreCase(propertyType)) {
                        prefs.getStringPreferences().put(propertyName, property.getValue().toString());
                    }
                    if ("int".equalsIgnoreCase(propertyType)) {
                        prefs.getIntegerPreferences().put(propertyName, getInteger(property));
                    }
                    if ("sit".equalsIgnoreCase(propertyType)) {
                        prefs.getScalableIntegerPreferences().put(propertyName, getInteger(property));
                    }
                    if ("flt".equalsIgnoreCase(propertyType)) {
                        prefs.getFloatPreferences().put(propertyName, getFloat(property));
                    }
                    if ("sft".equalsIgnoreCase(propertyType)) {
                        prefs.getScalableFloatPreferences().put(propertyName, getFloat(property));
                    }

                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return prefs;
    }

    private Integer getInteger(Entry<Object, Object> property) {
        Integer intVal = null;
        try {
            intVal = Integer.parseInt(property.getValue().toString());
        } catch (NumberFormatException e) {
            // not an int. could be a float, string or boolean
        }
        return intVal;
    }

    private Float getFloat(Entry<Object, Object> property) {
        Float floatVal = null;
        try {
            floatVal = Float.parseFloat(property.getValue().toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return floatVal;
    }

    private Boolean getBoolean(Entry<Object, Object> property) {
        Boolean boolVal = null;
        try {
            boolVal = Boolean.parseBoolean(property.getValue().toString());
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return boolVal;
    }

    public void saveUserPreferences(UserPreferences preferences) {

        try {
            Properties props = new Properties();

            for (Entry<String, Boolean> preference : preferences.getBooleanPreferences().entrySet()) {
                props.setProperty("bln." + preference.getKey(), preference.getValue().toString());
            }
            for (Entry<String, String> preference : preferences.getStringPreferences().entrySet()) {
                props.setProperty("str." + preference.getKey(), preference.getValue().toString());
            }
            for (Entry<String, Integer> preference : preferences.getIntegerPreferences().entrySet()) {
                props.setProperty("int." + preference.getKey(), preference.getValue().toString());
            }
            for (Entry<String, Integer> preference : preferences.getScalableIntegerPreferences().entrySet()) {
                props.setProperty("sit." + preference.getKey(), preference.getValue().toString());
            }
            for (Entry<String, Float> preference : preferences.getFloatPreferences().entrySet()) {
                props.setProperty("flt." + preference.getKey(), preference.getValue().toString());
            }
            for (Entry<String, Float> preference : preferences.getScalableFloatPreferences().entrySet()) {
                props.setProperty("sft." + preference.getKey(), preference.getValue().toString());
            }

            File f = new File(USER_PREFS_PATH);
            OutputStream out = new FileOutputStream(f);
            props.store(out, null);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
