/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.dodecahelix.clickfic.model.book.StoryQuery;
import com.dodecahelix.ifengine.data.story.LibraryCard;
import com.dodecahelix.ifengine.util.IoUtils;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class AssetLibraryHandler implements LibraryHandler {

    private static final String LIBRARY_FILE_PATH = "data/library.json";

    private Gson gson;

    public AssetLibraryHandler() {
        gson = new Gson();
    }

    @Override
    public List<LibraryCard> retrieveAdditionalStories(StoryQuery query) {

        try {
            String jsonString = IoUtils.readFileAsString(LIBRARY_FILE_PATH);

            Type storyMetadataListType = new TypeToken<Collection<LibraryCard>>() {}.getType();
            List<LibraryCard> stories = gson.fromJson(jsonString, storyMetadataListType);

            return stories;
        } catch (IOException e) {
            e.printStackTrace();
            return new ArrayList<LibraryCard>();
        }

    }

}
