/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic;

import java.util.List;

import com.dodecahelix.clickfic.model.book.StoryQuery;
import com.dodecahelix.ifengine.choice.ChoiceRecord;
import com.dodecahelix.ifengine.data.save.EnvironmentSave;
import com.dodecahelix.ifengine.data.serializer.StoryBundle;
import com.dodecahelix.ifengine.data.story.LibraryCard;
import com.dodecahelix.libgdx.template.config.Properties;
import com.dodecahelix.libgdx.template.config.UserPreferences;

public class DesktopPlatformDAO implements PlatformDAO {

    private EnvironmentHandler environmentHandler = new EnvironmentHandler();
    private SavedProgressHandler savedProgressHandler = new JsonSavedProgressHandler();
    private UserPreferencesHandler userPreferencesHandler = new UserPreferencesHandler();
    private LibraryHandler libraryHandler = new AssetLibraryHandler();

    @Override
    public boolean isMobile() {
        return false;
    }

    @Override
    public StoryBundle loadStoryFile(String storyName) throws PlatformException {
        return environmentHandler.loadStoryFile(storyName);
    }

    @Override
    public EnvironmentSave restoreSavedProgress(String name) throws PlatformException {
        return savedProgressHandler.restoreSavedProgress(name);
    }

    @Override
    public void saveProgress(EnvironmentSave environmentSaveData) throws PlatformException {
        savedProgressHandler.saveProgress(environmentSaveData);
    }

    @Override
    public UserPreferences loadUserPreferences() {

        return userPreferencesHandler.loadUserPreferences();
    }

    @Override
    public void saveUserPreferences(UserPreferences preferences) {

        userPreferencesHandler.saveUserPreferences(preferences);
    }

    @Override
    public List<LibraryCard> retrieveAdditionalStories(StoryQuery query) {
        return libraryHandler.retrieveAdditionalStories(query);
    }

    @Override
    public void saveChoiceRecord(List<ChoiceRecord> record) throws PlatformException {
        throw new IllegalStateException("not allowed to save turn record in Desktop mode (DEBUG MODE property should be false)");
    }

    @Override
    public List<ChoiceRecord> getChoiceRecord() {
        throw new IllegalStateException("not allowed to retrieve turn record in Desktop mode (DEBUG MODE property should be false)");
    }

    @Override
    public void initializeProperties(Properties properties) throws PlatformException {
    }

}
