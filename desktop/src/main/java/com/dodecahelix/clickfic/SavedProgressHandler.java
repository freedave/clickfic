/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic;

import com.dodecahelix.clickfic.PlatformException;
import com.dodecahelix.ifengine.data.save.EnvironmentSave;

public interface SavedProgressHandler {

    public EnvironmentSave restoreSavedProgress(String fileName) throws PlatformException;

    public void saveProgress(EnvironmentSave saveGame) throws PlatformException;

}
