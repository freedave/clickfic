/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic;

import java.net.URL;
import java.util.logging.LogManager;
import java.util.logging.Logger;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {

    static {
        String file = System.getProperty("java.util.logging.config.file");
        if (file == null || file.isEmpty()) {
            URL url = null;
            try {
                url = Main.class.getClassLoader().getResource("logging.properties");
                if (url == null) {
                    System.err.println("Cannot find logging.properties.");
                } else {
                    LogManager.getLogManager().readConfiguration(url.openStream());
                }
            } catch (Exception e) {
                System.err.println("Error reading logging.properties from '" + url + "': " + e);
            }
        }
    }

    public static void main(String[] args) {
        LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();

        cfg.title = "clickfic";
        cfg.useGL30 = false;
        //cfg.width = 480;
        //cfg.height = 600;
        cfg.width = 768;
        cfg.height = 1280;

        Logger log = Logger.getLogger(Main.class.getName());
        log.info("initializing Clickfic application in desktop mode");

        new LwjglApplication(new ClickFic(new DesktopPlatformDAO()), cfg);
    }
}
