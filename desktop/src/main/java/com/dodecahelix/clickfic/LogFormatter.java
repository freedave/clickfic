/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class LogFormatter extends Formatter {

    private static SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");

    @Override
    public String format(LogRecord record) {
        // Create a StringBuffer to contain the formatted record
        // start with the date.
        StringBuffer sb = new StringBuffer();

        // Get the level name and add it to the buffer
        sb.append("[");
        sb.append(record.getLevel().getName());
        sb.append("@");
        Date date = new Date(record.getMillis());
        sb.append(formatter.format(date));
        sb.append("] ");

        // Get the formatted message (includes localization
        // and substitution of paramters) and add it to the buffer
        sb.append(formatMessage(record));

        sb.append("\n");

        return sb.toString();
    }
}
