/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.library;

import android.content.Context;
import android.content.res.AssetManager;

import com.badlogic.gdx.Gdx;
import com.dodecahelix.clickfic.PlatformException;
import com.dodecahelix.ifengine.data.serializer.CsvStoryBundle;
import com.dodecahelix.ifengine.data.serializer.JsonStoryBundle;
import com.dodecahelix.ifengine.data.serializer.MediaFormat;
import com.dodecahelix.ifengine.data.serializer.StoryBundle;
import com.dodecahelix.ifengine.util.IoUtils;
import com.dodecahelix.libgdx.template.config.SharedConstants;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class EnvironmentDAO {

    private static final MediaFormat DATA_FORMAT = MediaFormat.JSON;

    private Context context;

    public EnvironmentDAO(Context context) {
        this.context = context;
    }

    public StoryBundle loadStoryFile(String storyName) throws PlatformException {

        StoryBundle story = null;
        switch (DATA_FORMAT) {
            case JSON:
                story = loadStoryJsonFile(storyName);
                break;
            case CSV:
                story = loadStoryCsvFile(storyName);
                break;
            default:
                throw new IllegalStateException("Data format not supported for story file : " + DATA_FORMAT);
        }

        return story;
    }

    private StoryBundle loadStoryJsonFile(String storyName) throws PlatformException {

        String fullStoryName = "data/" + storyName + ".json";
        Gdx.app.debug(SharedConstants.LOGGING_TAG_LIFECYCLE, "Loading story file " + fullStoryName);

        try {
            AssetManager am = context.getAssets();
            InputStream is = am.open("data/" + storyName + ".json");

            String jsonString = IoUtils.readFileFromInputStreamAsString(is);

            Gdx.app.debug(SharedConstants.LOGGING_TAG_LIFECYCLE, "Story file content : " + jsonString);
            return new JsonStoryBundle(jsonString);
        } catch (IOException e) {
            throw new PlatformException(e);
        }
    }

    @Deprecated
    public StoryBundle loadStoryCsvFile(String dataFileName) throws PlatformException {
        List<String> dataFile = new ArrayList<String>();

        BufferedReader buffreader = null;
        try {
            AssetManager am = context.getAssets();
            InputStream is = am.open("data/" + dataFileName + ".csv");

            // read the file from an IS
            InputStreamReader inputreader = new InputStreamReader(is);
            buffreader = new BufferedReader(inputreader);
            String line;
            while ((line = buffreader.readLine()) != null) {
                dataFile.add(line);
            }

        } catch (IOException e) {
            throw new PlatformException(e);
        } finally {
            // close the buffered reader to prevent memory leakage
            if (buffreader != null) {
                try {
                    buffreader.close();
                } catch (IOException e) {
                    // fucking annoying checked exception
                    e.printStackTrace();
                }
            }
        }

        return new CsvStoryBundle(dataFile);
    }


}
