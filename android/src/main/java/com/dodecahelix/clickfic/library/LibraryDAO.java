/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.library;

import java.util.List;

import com.dodecahelix.clickfic.PlatformException;
import com.dodecahelix.clickfic.model.book.StoryQuery;
import com.dodecahelix.ifengine.data.story.LibraryCard;

public interface LibraryDAO {

    List<LibraryCard> retrieveAdditionalStories(StoryQuery query) throws PlatformException;

}
