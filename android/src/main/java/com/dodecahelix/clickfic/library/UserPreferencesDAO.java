/*******************************************************************************
 * Copyright (c) 2015 David Peters
 * <p/>
 * ClickFic is dual licensed under GPLv3 and Commercial licenses.
 * The details of these terms are listed in the LICENSE.txt
 * which is located at the base of this project's source repository.
 *******************************************************************************/
package com.dodecahelix.clickfic.library;

import java.util.Map.Entry;

import com.dodecahelix.libgdx.template.config.UserPreferences;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

public class UserPreferencesDAO {

    private Context context;

    public UserPreferencesDAO(Context context) {
        this.context = context;
    }

    public UserPreferences loadUserPreferences() {
        UserPreferences userPrefs = new UserPreferences();
        SharedPreferences androidPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        if (androidPrefs != null && androidPrefs.getAll() != null) {
            for (Entry<String, ?> entry : androidPrefs.getAll().entrySet()) {
                if (entry.getValue() instanceof String) {
                    userPrefs.getStringPreferences().put(entry.getKey(), (String) entry.getValue());
                }
                if (entry.getValue() instanceof Integer) {
                    if (!entry.getKey().startsWith("s.")) {
                        userPrefs.getIntegerPreferences().put(entry.getKey(), (Integer) entry.getValue());
                    } else {
                        userPrefs.getScalableIntegerPreferences().put(entry.getKey().substring(2), (Integer) entry.getValue());
                    }
                }
                if (entry.getValue() instanceof Boolean) {
                    userPrefs.getBooleanPreferences().put(entry.getKey(), (Boolean) entry.getValue());
                }
                if (entry.getValue() instanceof Float) {
                    if (!entry.getKey().startsWith("s.")) {
                        userPrefs.getFloatPreferences().put(entry.getKey(), (Float) entry.getValue());
                    } else {
                        userPrefs.getScalableFloatPreferences().put(entry.getKey().substring(2), (Float) entry.getValue());
                    }
                }
            }
        }
        return userPrefs;
    }

    public void saveUserPreferences(UserPreferences preferences) {

        SharedPreferences androidPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        Editor edit = androidPrefs.edit();

        for (Entry<String, Boolean> preference : preferences.getBooleanPreferences().entrySet()) {
            if (preference.getValue() != null) {
                edit.putBoolean(preference.getKey(), preference.getValue());
            }
        }
        for (Entry<String, String> preference : preferences.getStringPreferences().entrySet()) {
            if (preference.getValue() != null) {
                edit.putString(preference.getKey(), preference.getValue());
            }
        }
        for (Entry<String, Float> preference : preferences.getFloatPreferences().entrySet()) {
            if (preference.getValue() != null) {
                edit.putFloat(preference.getKey(), preference.getValue());
            }
        }
        for (Entry<String, Integer> preference : preferences.getIntegerPreferences().entrySet()) {
            if (preference.getValue() != null) {
                edit.putInt(preference.getKey(), preference.getValue());
            }
        }
        for (Entry<String, Integer> preference : preferences.getScalableIntegerPreferences().entrySet()) {
            if (preference.getValue() != null) {
                edit.putInt("s." + preference.getKey(), preference.getValue());
            }
        }
        for (Entry<String, Float> preference : preferences.getScalableFloatPreferences().entrySet()) {
            if (preference.getValue() != null) {
                edit.putFloat("s." + preference.getKey(), preference.getValue());
            }
        }

        edit.apply();
    }

}
